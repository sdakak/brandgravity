In the design document, I had originally envisioned that I would use the driving distance between each location pair as this would be more representative of how consumers would get to stores. However, because of the sheer number of queries that needed to be made (2.5 billion for 50k stores), this was quickly dismissed as it would hit the API limit for any commercial provider very quickly.

The next task was to figure out how to measure straight-line (as the crow flies) distances between locations most accurately. However, on a sphere there are no straight lines in the Euclidean sense. To measure distances on a sphere we use geodesics. Geodesics or great circles are circles whose center coincides with the center of earth. There is a unique great circle between any two points on the earth, and the shorter arc measures the straight-line distance.

However, earth is not a perfect sphere but a spheroid, so the formulas used to calculate great-circle distance have varying degree of accuracy. Some of these formulas are accurate over short distances, others over long distances, others between certain latitudes, etc. This has to do with the shape of the earth, the way the coordinate system is laid on top of it to approximate this shape, and the way the formula negotiates this compromise. 

After some research, I decided to test how stable the Haversine formula <<haversine, (Veness)>> was for the united states (more accurately the lat/long that the US falls in) and the type of distances I wanted to measure. I used the Haversine implementation in the <<simplelatlng-section, nifty simplelatlng library>>.

The Haversine formula is as follows:

image::assets/haversine-formula.png[Haversine Long Distances Test]

where

* r is the radius of earth,
* phi1, phi2: latitude of point 1 and latitude of point 2
* lamba1, lambda2: longitude of point 1 and longitude of point 2

==== Ground Truth
To test the distances calculated using the Haversine formula I needed to have some other source to compare it against. I decided to test the Haversine formula with the distances reported by Google Maps. The challenge here is that Google Maps maybe less accurate than Haversine or that it internally uses Haversine too which would render this test useless.

However, despite those objections, I still needed some other source to test my distances against and this seemed to be the most reliable source. And both those concerns were quickly found to be unwarranted. It was not likely that Google Maps was less accurate at calculating distances than my naive attempt at Haversine. And because the distances it reported were different from the ones I calculated, that was a positive indication that Google Maps was not using Haversine internally.

==== Haversine long distances test

[[haversine-long-distances]]
.Haversine Long Distances Test
image::assets/city-distances.png[Haversine Long Distances Test]

- Take the 7 most populous cities in the US, and find the distances between them using Google Maps and the Haversine formula
- As the 7 most populous cities are spread out nicely around the US, this allows us to test city-level distances nicely. The furthest pair, Philadelphia and Phoenix, is 2000 miles away and the nearest pair, Chicago and Columbus, is just 300 miles away.
- <<haversine-long-distances,Draw a line on Google Maps>> between each city pair for a total of 21 lines and record the distance measured
- Calculate the distance between the same pairs using the Haversine formula and <<haversine-long-distances-error, calculate the error percentage>>
- The test shows that the average percentage error is about 1.19% and the variance is very tight too.

<<<

[[haversine-long-distances-error]]
.Haversine Long Distances Error
[options="header"]
|===
|City 1|Coordinates|City 2|Coordinates|Actual Distance Recorded|Distance Calculated by Code|Percentage Error|Absolute Value
|Charlotte|35.2087,-80.8307|Philadelphia|40.0094,-75.1333|446.3|455.0654153|1.964018669|1.964018669
|Charlotte|35.2087,-80.8307|Chicago|41.8376,-87.6818|586.32|588.575398|0.38467014|0.38467014
|Charlotte|35.2087,-80.8307|Houston|29.7805,-95.3863|918.53|926.2131272|0.836459035|0.836459035
|Charlotte|35.2087,-80.8307|Phoenix|33.5722,-112.088|1777.41|1778.443399|0.058140741|0.058140741
|Charlotte|35.2087,-80.8307|Columbus|39.9848,-82.985|360.36|350.4054053|-2.762402803|2.762402803
|Charlotte|35.2087,-80.8307|El Paso|31.8484,-106.427|1490.5|1488.210671|-0.153594718|0.153594718
|Philadelphia|40.0094,-75.1333|Chicago|41.8376,-87.6818|665.32|666.5313287|0.182067074|0.182067074
|Philadelphia|40.0094,-75.1333|Houston|29.7805,-95.3863|1329.2|1343.008286|1.038841879|1.038841879
|Philadelphia|40.0094,-75.1333|Phoenix|33.5722,-112.088|2069.68|2077.009654|0.354144322|0.354144322
|Philadelphia|40.0094,-75.1333|Columbus|39.9848,-82.985|433.65|415.466378|-4.193156226|4.193156226
|Philadelphia|40.0094,-75.1333|El Paso|31.8484,-106.427|1831.23|1828.461494|-0.15118284|0.15118284
|Chicago|41.8376,-87.6818|Houston|29.7805,-95.3863|963.83|937.2863289|-2.753978516|2.753978516
|Chicago|41.8376,-87.6818|Phoenix|33.5722,-112.088|1466.7|1444.754856|-1.496225801|1.496225801
|Chicago|41.8376,-87.6818|Columbus|39.9848,-82.985|281.7|276.5908924|-1.81366973|1.81366973
|Chicago|41.8376,-87.6818|El Paso|31.8484,-106.427|1245.54|1240.929234|-0.370182125|0.370182125
|Houston|29.7805,-95.3863|Phoenix|33.5722,-112.088|1042.27|1015.106783|-2.606159305|2.606159305
|Houston|29.7805,-95.3863|Columbus|39.9848,-82.985|998.82|993.6326088|-0.519351956|0.519351956
|Houston|29.7805,-95.3863|El Paso|31.8484,-106.427|682.55|670.2165859|-1.806961265|1.806961265
|Phoenix|33.5722,-112.088|Columbus|39.9848,-82.985|1664.71|1662.154778|-0.153493522|0.153493522
|Phoenix|33.5722,-112.088|El Paso|31.8484,-106.427|353.5|349.9344435|-1.008643994|1.008643994
|Columbus|39.984,-82.985|El Paso|31.8484,-106.427|1427.17|1421.583891|-0.391411638|0.391411638
7+| Total Percentage Error|#1.19#
|===

<<<

==== Haversine short distances test

The above test measures how well the Haversine formula does at city-level distances. But what about distances within a city, the ones that my system was more likely to be dealing with. For this I used the following test. 

[[haversine-short-distances]]
.Haversine Short Distances Test
image::assets/close-distances.png[Haversine Short Distances Test]

- Take 10 points on Google maps around Villanova, PA at a distance of 0.2 - 128 and <<haversine-short-distances, record the distance between them.>>
- Calculate the distance using Haversine and calculate the percentage error when compared with the distances reported by Google Maps
- Here the <<haversine-short-distances-error, average error is about 23%>>, an alarmingly high number. However, if we look at percentage error for distances more than 10 miles away its less than 1%. As a large number of distances are going to be more than 10 miles away, this error can be overlooked.

<<<

[[haversine-short-distances-error]]
.Haversine Short Distances Error
[options="header"]
|===
|Villanova coordinates| Other point coordinates|Distance Recorded|Distance Calculated|Percentage Error|Abs Value
|40.035764,-75.337399|40.03479, -75.33759|0.17|0.068051297|59.96982506|59.96982506
|40.035764,-75.337399|40.03454, -75.33769|0.31|0.08596004|72.27095489|72.27095489
|40.035764,-75.337399|40.03395, -75.33795|0.76|0.128680345|83.06837561|83.06837561
|40.035764,-75.337399|40.0082, -75.3345|2|1.910658279|4.46708607|4.46708607
|40.035764,-75.337399|39.9903, -75.299|4|3.741207616|6.469809592|6.469809592
|40.035764,-75.337399|39.991, -75.202|8.08|7.804124556|3.414300052|3.414300052
|40.035764,-75.337399|39.991, -75.042|15.98|15.93503044|0.281411489|0.281411489
|40.035764,-75.337399|40.277, -75.45|32.3|32.3649|-0.200928793|0.200928793
|40.035764,-75.337399|40.973, -75.180|64.21|65.28262472|-1.670494815|1.670494815
|40.035764,-75.337399|40.801, -77.60|128.78|130.2294476|-1.125522283|1.125522283
5+|Total Percentage Error| 23.29387087
5+|Total Percentage Error < 10 miles | 38.27672521
5+|Total Percentage Error > 10 miles | #0.819589345#
|===