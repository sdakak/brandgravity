User Stories <<user-stories,(Cohn)>> are a popular way to capture requirements used in Agile software development <<solid,(Martin)>>. These are a collection of features that need to be developed for the next iteration of the program. They are often phrased as a simple task that a certain user role needs the system needs to perform.

=== Ranking Rental Location

The raison d'être for this program is to help new franchisees choose which store location to open their new store in from a given list of locations they are considering. The following requirements are the heart of the project.

As a new business owner:

* I want a metric for my prospective rental location indicating its desirability.
* I want to have my prospective rental locations ranked in order of desirability.
* I want to see a visualization explaining this ranking.
* I want tips on interpreting this visualization.
* I want tips on how to use the visualization to compare my prospective rental locations.

Please see <<rental-report-section,Rental Report Section>> to see how these are satisfied.

=== Advanced analyses 

This program can also be used by data scientists and analysts who would like to dive deeper into figuring out what sorts of clusters the stores in an area form.

As an analyst:

* I want the system to come preloaded with at least 10 brands with 10 locations each (100 total stores) in a 
single region.
** The system comes preloaded with 100 stores around Philadelphia.

* I want the system to come with stores in three distinct regions in USA that I can use as a starting point of my analyses
** A spreadsheet with stores in three regions is included. See <<data-collection-section,Data Collection Section.>>

<<<

* I want to be able to load my own file containing store locations
** This as simple as creating a csv file that follows the format specified in <<input-file-section, Input File Section>>
** This file is loaded as follows:
----
FileNames fileNames = new FileNames("myFile");
ArrayList<Brand> allBrands = Reader.readInputFile(fileNames.getInputFile());
----
 
* I want to see the average driving distance between any two brands
`brandDistances.getDetailedStats("CVS", "KFC")`
----
KFC and CVS Stats:
Mean distance: 4.61
Median: 3.64
Range: 11.94 (0.13 - 12.07)
Interquartile range: 3.85
Variance: 15.31
Standard Deviation: 3.91
----

* I want to see the overall top three brands that occur closest to each other and top three that occur furthest from each other
`brandDistances.getClosestBrands()`
----
StarBucks, StarBucks, 0.28
StarBucks, Wendys, 0.54
StarBucks, AppleBees, 0.69
----

`brandDistances.getFurthestBrands();`
----
CVS, Walmart, 6.93
StarBucks, Walmart, 6.54
Walmart, Domino's, 6.54
----

* I want to see the top three brands that occur closest to a given brand and top three that occur furthest from it
** This works just like the previous user story

* I want to filter stores to only include those in a certain state
----
ArrayList<String> filterStates = new ArrayList<String>();
filterStates.add("NY");
filterStates.add("GA");

FilterConditions filterConditions = new FilterConditions();
filterConditions.setStates(filterStates);

// Filtering locations. This code allows only locations in NY and GA
Filter.filterBrands(allBrands2, filterConditions);
----

* I want to filter stores to only include those that belong to a certain brand 
** This works just like the previous user story.
