=== Overview

[[concept-overview-diagram]]
.Concept Diagram: Overview
image::assets/concept-overview.png[Concept Diagram: Overview]

The system starts with taking a single string `stores.csv` which represents the input csv file containing all stores (see sample below). It then reads the file into a List of all stores called `allBrands`. This is then used to <<calculate-average-distance-between-brands-section, Calculate average distance between brands>> which is stored as `brandDistances`

<<<

[[input-file-section]]
.stores.csv (sample input file)
----
"McDonald's","39.95,-75.16","1617 John F Kennedy Boulevard, Philadelphia","Philadelphia","PA","19103" // <1>
"McDonald's","39.95,-75.20","3935 Walnut Street, Philadelphia","Philadelphia","PA","19104"
"McDonald's","39.93,-75.16","914 South Broad Street, Philadelphia","Philadelphia","PA","19146" // <2>
...
"KFC","39.95,-75.15","Gallery Market East, Ninth And Market Streets, Philadelphia","Philadelphia","PA","19107"  // <3>
...
"CVS","40.19,-75.10","7 York Road, Warminster","Warminster","PA","18974"
... // <4>
----
<1> Fields are Brand Name, Coordinates, Address, City, State, Zip
<2> First listing all stores of a particular brand (in this case McDonalds)
<3> Listing all the stores of a second brand
<4> This file will contain a complete listing of all known stores

The system also takes the filename of a csv file that contains prospective rental locations (`rentals.csv`) and uses that to make a `rentalLocations` object.

.rentalLocations.csv (sample rental locations file)
----
"se_philly","5000","McDonald's","39.92,-75.16","1701 S 9th St","Philadelphia","PA","19148" // <1>
"central_philly","6500","McDonald's","39.94,-75.16","1305 Catharine St","Philadelphia","PA","19147"
"nw_philly","4500","McDonald's","39.95,-75.17","2102 Market St","Philadelphia","PA","19103" // <2>
----
<1> Fields are Rental Location Name, Rent, Prospective Brand, Coordinates, Street Address, City, State, Zip
<2> This file shows that the user wants to open a McDonalds store and he is considering these three locations

The `brandDistances` and `rentalLocations` object from the previous steps are fed into the <<predict-rental-location-suitability-section, predict rental location suitability step>> which ultimately generates a http://sdakak.bitbucket.org/brandgravity/report.html[visual report]. This report contains the ranking of the prospective rental locations, an explanation of why they were selected and a map illustrating this. See <<rental-report-section, Rental Locations Report Section for details.>>

<<<

[[calculate-average-distance-between-brands-section]]
=== Calculate Average Distance Between Brands

[[concept-calculate-average-distance-between-brands-diagram]]
.Concept Diagram: Calculate Average Distance Between Brands
image::assets/concept-calculateAvgDistance.png[Concept Diagram: Calculate Average Distance Between Brands]

This step takes in a List of Brands (`allBrands`) and generates the average distance between brands (`brandDistances`)

. `writePermutations()`: This generates a `permutationFile` (see below for sample) that contains the permutation of every store with every other store.

* Algorithm: Until all store locations are permuted, make a new pair, get the straight-line distance, write to csv file.

. `writeClosestPair()`: This step generates a `closestPairFile` (see below) that contains a list of every store of every brand that is closest to a given store.

* Algorithm: Until same brands are being read (as you have to compare all stores of brandB to pick the one that is closest), update closestPair if new pair is closer than the current minimum, write the current minimum to file when new brand pair begins.

. `calculateAverageDistance()`: This step generates a `avgDistanceFile` (see below) and a `brandDistances` data structure which contain the average driving distance between two brands

* Algorithm: Until same brands are being read from the `closestPairFile`, add distance to average, write current average to file when new pair begins.

<<<

.storesPermutation.csv (sample permutation file)
----
"McDonald's","(39.95,-75.16)","KFC","(39.95,-75.15)","0.58" // <1>
"McDonald's","(39.95,-75.16)","KFC","(39.97,-75.15)","1.30"
"McDonald's","(39.95,-75.16)","KFC","(39.93,-74.97)","10.28" // <2>
...
"McDonald's","(39.95,-75.20)","KFC","(39.95,-75.15)","2.46" // <3>
... // <4>
"McDonald's","(39.95,-75.16)","CVS","(40.19,-75.10)","16.68" // <5>
...
"McDonald's","(39.95,-75.16)","Subway","(39.95,-75.17)","0.60" // <6>
... // <7>
----
<1> Fields are Store1 Brand, Store1 Coordinates, Store2 Brand, Store2 Coordinates, Distance between these stores
<2> First permuting Store1 of interested brand (McDonalds) with all stores of second brand (KFC)
<3> Now permuting Store2 of interested brand (McDonalds) with all stores of second brand (KFC)
<4> Permuting all stores of interested brand (McDonalds) with all stores of second brand (KFC)
<5> Permuting all stores of interested brand (McDonalds) with a third brand (CVS)
<6> Permuting all stores of interested brand (McDonalds) with yet another brand (Subway)
<7> Permuting all stores of interested brand (McDonalds) with all stores of every other brand

.storesClosestPair.csv (sample closest pair file)
----
"McDonald's","(39.95,-75.16)","KFC","(39.95,-75.15)","0.58" // <1>
"McDonald's","(39.95,-75.20)","KFC","(39.94,-75.23)","1.96"
"McDonald's","(39.93,-75.16)","KFC","(39.95,-75.15)","1.05"
... // <2>
"McDonald's","(39.95,-75.16)","CVS","(39.95,-75.16)","0.24" // <3>
...
"McDonald's","(39.95,-75.16)","Subway","(39.95,-75.16)","0.18" // <4>
... // <5>
----
<1> Fields are Store1 Brand, Store1 Coordinates, Store2 Brand, Store2 Coordinates, Distance between stores.
<2> Listing closest store of first brand (KFC) from every store of interested brand (McDonalds).
<3> Listing closest store of second brand (CVS) from every store of interested brand (McDonalds).
<4> Listing closest store of third brand (Subway) from every store of interested brand (McDonalds).
<5> Listing closest stores of all brands from every store of interested brand (McDonalds).

<<<

.storesAverageDistance.csv (sample average distance file)
----
"McDonald's","KFC","1.09" // <1>
"McDonald's","CVS","0.98"
"McDonald's","Subway","1.02"
... // <2>
"KFC","CVS","4.61" // <3>
...
"CVS","Subway","1.43" // <4>
... // <5>
----
<1> Fields are Brand1, Brand2, Average Distance
<2> Listing Average Distance of all brands from Brand1 (McDonalds)
<3> Listing Average Distance of all brands from Brand2 (KFC)
<4> Listing Average Distance of all brands from Brand3 (CVS)
<5> Listing Average Distance of all brands from all brands

<<<

[[predict-rental-location-suitability-section]]
=== Predict Rental Location Suitability

[[concept-predict-rental-location-suitability-diagram]]
.Concept Diagram: Predict Rental Location Suitability
image::assets/concept-predictRentalLocation.png[Concept Diagram: Predict Rental Location Suitability]

This step takes in `rentalLocations` and `brandDistances` and ranks the rental location by suitability

. Calculate Rental Location Distances: This step takes in `rentalLocations` which were created from a csv file and populates `rentalLocations.distanceToOtherBrands` with the closest store of every other brand for the given prospective location

* Algorithm: For each rental `Location` make it into a pseudo brand with just one store (itself), use that to calculate average distance of this rental brand with every other brand, store distance in `rentalLocations.distanceToOtherBrands`.

. Rank Rental Locations: This steps takes in `rentalLocations` and `brandDistances` and populates 
`rentalLocation.diffMatrix` with the difference in distance between this rental `Location` and the average distance of brands in `brandDistances`

* Algorithm: For every brand, get the average distance of that brand from the rental location brand, and then subtract it from the actual distance of the rental location and closest store of this brand. Store this result in `rentalLocations.diffMatrix`

