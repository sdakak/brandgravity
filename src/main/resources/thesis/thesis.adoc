= Why Are Things Where They Are http://sdakak.bitbucket.org/brandgravity/thesis.pdf[PDF]
Nakul Rathod <nakulrathod@gmail.com>
v1.0, 2014-12-03
// Settings
:toc: left
:doctype: book
// :data-uri:
:sectanchors:
:sectnums:
:pagenums:
:source-highlighter: coderay

:numbered!:
[preface]
== Acknowledgments

include::acknowledgements.adoc[]

<<<

:numbered!:
[abstract]
== Abstract

The type of stores in an area gives us great insight into the people who live there. The geographic distribution of stores can be used to solve a variety of problems. Towards my goal of leveraging these patterns to solve novel problems, I have built a system to help new business owners figure out the most desirable place to open their store. I have also come up with a visualization that communicates the brands that any specific store likes to be near (or far from). This brand affinity can be used to rank the desirability of prospective rental locations. I present an analysis of how this idea went from conception to its final form with commentary on challenges faced and lessons learned. My hope is that this project not only serves as an interesting GIS project, but also a useful case study of implementing a complex software engineering project using Agile Development Methodology.

<<<


:numbered:
[[introduction-section]]
== Introduction

include::introduction.adoc[]

[[rental-report-section]]
== Prospective Rental Locations Report
include::reportForThesis.adoc[]
//include::reportForThesis-pdf.adoc[]

<<<

== User Stories

include::userStories.adoc[]

<<<

== Marketecture

include::marketecture.adoc[]

<<<

[[concept-diagram-section]]
== Concept Diagram

include::concept-diagram.adoc[]

<<<

== Architecture

include::architecture.adoc[]

<<<

[[data-collection-section]]
== Data Collection

include::data-collection.adoc[]

<<<

[[algorithm-section]]
== Algorithm

include::algorithm.adoc[]

<<<
[[testing-section]]
== Testing

include::testing.adoc[]

[[distance-section]]
=== Haversine testing

include::haversine-testing.adoc[]

<<<

== Future Work

include::future-work.adoc[]

<<<


:numbered!:
[sect1]
== Bibliography
	
- [[testing]] Ammann, Paul, et al. Introduction to Software Testing. Cambridge University Press, 2008. Print.
- [[man-month]] Brooks, Frederick. The Mythical Man-Month: Essays on Software Engineering, Anniversary Edition. Addison-Wesley Professional, 1995. Print.
- [[user-stories]] Cohn, Mike. User Stories Applied: For Agile Software Development. Addison-Wesley Professional, 2004. Print.
- [[uml]] Fowler, Martin. UML Distilled: A Brief Guide to the Standard Object Modeling Language. Addison-Wesley Professional, 2003. Print.
- [[refactoring]] Fowler, Martin, et al. Refactoring: Improving the Design of Existing Code. Addison-Wesley Professional, 1999. Print.
- [[design-patterns]] Gamma, Erich, et al. Design Patterns: Elements of Reusable Object-Oriented Software. Addison-Wesley Professional, 1999. Print.
- [[junit]] Hunt, Andy, et al. Pragmatic Unit Testing in Java with JUnit. The Pragmatic Programmers, 2003. Print.
- [[architecture]] Gorton, Ian. Essential Software Architecture. Springer, 2011. Print.
- [[grasp]] Larman, Craig. Applying UML and Patterns: An Introduction to Object-Oriented Analysis and Design and Iterative Development. Prentice Hall, 2004. Print.
- [[solid]] Martin, Robert. Agile Software Development, Principles, Patterns, and Practices. Prentice Hall, 2002. Print.
- [[complexity-metric]] McConnell, Steve. Code Complete: A Practical Handbook of Software Construction. Microsoft Press, 2004. Print
- [[haversine]] Veness, Chris. Calculate distance and bearing between two Latitude/Longitude points using Haversine formula in JavaScript. Web.
	
<<<