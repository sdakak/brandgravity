## Finding distance between two locations
In the design document, I had originally envisioned that I would use the driving distance between each location pair as this would be more representative of how consumers would get to stores. However, because of the sheer number of queries that needed to be made (2.5 billion for 50k stores), this was quickly dismissed as it would hit the API limit for any commercial provider very quickly.

The next task was to figure out how to measure straight-line (as the crow flies) distances between locations most accurately. However, on a sphere there are no straight lines (in the Euclidean sense). To measure distances on a sphere we use geodesics. Geodesics or great circles are circles whose center coincides with the center of earth. There is a unique great circle between any two points on the earth, and the shorter arc measures the straight-line distance.

However, earth is not a perfect sphere but a spheroid, so the formulas used to calculate great-circle distance have varying degree of accuracy. Some of these formulas are accurate over short distances, others over long distances, others between certain latitudes, etc. This has to do with the shape of the earth, the way the coordinate system is laid on top of it to approximate this shape, and the way the formula negotiates this compromise. 

After some research, I decided to test how stable the haversine formula was for the united states (more accuratly the lat/long that the US falls in) and the type of distances I wanted to measure.

## Ground Truth
To test the distances calculated using the haversine formula I needed to have some other measure to compare it against. I decided to test the haversine formula with the distances reported by Google Maps. The challenge here is that Google Maps maybe less accurate than Haversine or that it internally uses Haversine too.

However, I needed some other measure to test my distances against and this seemed to be the most reliable source. And both those concerns were quickly found to be unwarranted. It wasn't likely that Google Maps was less accurate at calculating distances than my naive attempt at Haversine. And because the distances it reported were different from the ones I calculated, that was a positive indication that Google Maps wasn't using Haversine internally.

## Haversine long distances test
- Take the 7 most populous cities in the US, and find the distances between them using Google Maps and Haversine
- As the 7 most populous cities are spread out nicely around the US, this allows us to test city-level distances nicely. The furthest pair, Philadelphia and Phoenix, is ~2000 miles away and the nearest pair, Chicage and Columbus, is just ~300 miles away.
- Draw a line on Google Maps between each city pair for a total of 21 lines and record the distance measured [city_distances.png]
- Calculate the distance between the same pairs (and same coordinates) using the Haversine formula and calculate the error percentage [city_distances.xlsx]
- The test shows that the average percentage error is about 1.19% and the variance is very tight too.

## Haversine short distances test
- Take 10 points on Google maps around Villanova, PA at a distance of 0.2 - 128 and record the distance between them [close_distances.png]
- Calculate the distance using Haversine and calculate the percentage error
- This test allows us to measure how well the algorithm will do in typical in-city distances that the program well rely on most often
- Here the average error is about 23%, an alarmingly high number [close_distances.xlsx]. However, if we look at percentage error for distances more than 10 miles away its less than 1%. As a large number of distances are going to be more than 10 miles away, this error can be overlooked.

