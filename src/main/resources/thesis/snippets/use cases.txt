Access to at least 10 brands with 10 locations each (100 total stores) in a single region.

Load user's own file containing store locations

See average driving distance between any 2 brands

See top three brands that occur closest to a given brand and top three that occur furthest from it

See overall top three brands that occur closest to each other and top three that occur furthest from each other

Filter stores to only include those that belong to a certain brand or those in a certain state



List in priority order

### User stories. Short. Focus on intentions. Placeholder for conversation.
As a <role>
I want <goal>
so that <reason>

As a User
I want to search by keywoard
so that I can find relevant articles

---

### Use cases. Record of conversation
Title: <phrase, verb>
Register new meber 
Transfer funds
Purchase items

Actor: User, customer, member, another system, anything that acts on our system

Scenario: Numbered list of things to do

Extensions: Alternative flows

Precondition: What must be true to start this use case

---
-conceptual model
-classic oo mistake. Giving an actor too much responsibility
-god class
-System validates payment, system will send order by email. Some part of System, do not make a system object
-Responsibilities should be distributed among objects. Objects should be responsible for themselves
S - Single Responsibility Principle
O - Open / Closed Principle. Open for extension, closed for modification
D - Dependency Inversion Principle. Depend on abstractions, not on concretions.
---



-fragile, breakable code, tedious to implement feature
DRY: Don't repeat yourself. Combinatorics.
YAGNI: You Ain't Gonna Need It (solve today's problems)
--Code smells:
long methods,
collection obsession
long comments
Feature envy: Class does very little except use the methods of another class

General Responsibility Assignment Software Patterns

Expert / Information Expert. Classes need to be responsible for themselves. Customer, Shopping Cart Item. Customer wants to know total.
Low coupling / high cohesion: Reduce dependencies
Coupling: level of dependencies between objects
Cohesion: Level that class contains focused, related behaviors
Pure Fabrication. Create new class for behavior that doesn't fit anywhere else, rather than reduce cohesion.
Indirection: Reduce coupling
Protected Variations. Protect system from changes and variations. Identify most likely points of change
G
R
A
S
P
