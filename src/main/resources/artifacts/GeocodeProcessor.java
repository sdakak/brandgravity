/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package geocodeprocessor;

/**
 *
 * @author Rohit Kumar
 */
import java.net.*;
import org.xml.sax.InputSource;
import org.w3c.dom.*;
import javax.xml.xpath.*;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.*;
import java.nio.*;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import org.xml.sax.SAXException;

public class GeocodeProcessor {

  // Default URL prefix to the geocoder
  // The URL shown in these examples is a static URL which should already
  // be URL-encoded. In practice, you will likely have code
  // which assembles your URL from user or web service input
  // and plugs those values into its parameters.
    //private static final String GEOCODE_REQUEST_PREFIX = "https://maps.googleapis.com/maps/api/place/nearbysearch/xml?location=39.949753,-75.163178&radius=804672&name=McDonalds&types=food&sensor=false&key=AIzaSyDzIYwTrjbv2uG0k5paPn1-6j1mB4Ho_TA";
    private static final String GEOCODE_REQUEST_PREFIX2= "https://maps.googleapis.com/maps/api/place/details/xml?reference=";
    private static final String GEOCODE_QUERY= "&sensor=true&key=AIzaSyDzIYwTrjbv2uG0k5paPn1-6j1mB4Ho_TA";
  //private static final String GEOCODE_QUERY = "?location=39.949753,-75.163178&radius=804672&name=CVS&types=food&sensor=false&key=AIzaSyDzIYwTrjbv2uG0k5paPn1-6j1mB4Ho_TA";
    private static final String GEOCODE_REQUEST_PREFIX ="https://maps.googleapis.com/maps/api/place/radarsearch/xml?location=39.949753,-75.163178&radius=804672&name=McDonalds&types=food&sensor=false&key=AIzaSyDzIYwTrjbv2uG0k5paPn1-6j1mB4Ho_TA";
  // Note: The default XPath expression "/" selects
  // all text within the XML.
    //private static String XPATH_EXPRESSION = "/PlaceSearchResponse//geometry/location/lng/text()";
    private static String XPATH_EXPRESSION = "/PlaceSearchResponse//reference/text()";
  //private static String XPATH_EXPRESSION = "/PlaceSearchResponse//vicinity/text()";
    private static String XPATH_EXPRESSION2 = "/PlaceDetailsResponse//name/text()";
  
  public String _xpath = null;
  public Document _xml = null;

  public static void main(String[] args) throws IOException, URISyntaxException, ParserConfigurationException, SAXException {

    //BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

    String inputQuery, resultXml, urlString,urlString2;// xPathString = null;
    
    ByteBuffer resultBytes = null;

    // For testing purposes, allow user input for the URL.
    // If no input is entered, use the static URL defined above.    
    //System.out.println("Enter the Geocode Request (default is 'New York, NY'): ");
    //inputQuery = input.readLine();
    //if (inputQuery.equals("")) {
    //  inputQuery = GEOCODE_QUERY;
    //}
    
    urlString = GEOCODE_REQUEST_PREFIX;// +  GEOCODE_QUERY ;
    //System.out.println(urlString);

    // Convert the string to a URL so we can parse it
    URL url = new URL(urlString);

//    // For testing purposes, allow user input for the XPath expression.
//    // If no input is entered, use the default expression defined above.   
//    System.out.println("Enter the XPath expression to evaluate the response (default is '//text()'): ");
//    xPathString = input.readLine();
//    if (xPathString.equals("")) {
//      xPathString = XPATH_EXPRESSION;
//    }

    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
    
    Document geocoderResultDocument = null;
    try {
      // open the connection and get results as InputSource.
      conn.connect();
      InputSource geocoderResultInputSource = new InputSource(conn.getInputStream());

      // read result and parse into XML Document
      geocoderResultDocument = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(geocoderResultInputSource);
    } finally {
      conn.disconnect();
    }

    // Process the results
    NodeList nodes = process(geocoderResultDocument, XPATH_EXPRESSION);
    List <String> nodeString=new ArrayList();
    // Print results
    int i;
    int c= nodes.getLength();
    for (i = 0; i < nodes.getLength(); i++) {
      nodeString.add(nodes.item(i).getTextContent());
//      System.out.print(nodeString.get(i));
//      System.out.print("\n");
    }
    
    for(int j=0;j<c;j++){
        //System.out.println("ref="+nodeString.get(j));
    urlString2 = GEOCODE_REQUEST_PREFIX2+ URLEncoder.encode(nodeString.get(j),"UTF-8") +  GEOCODE_QUERY ;
    //System.out.println(urlString2);
        detailsResult(urlString2);
    
    }
    
  }
  
  private void GeocodeProcessor() {
  }
  
  public static void detailsResult(String urlString2)throws IOException, URISyntaxException, ParserConfigurationException, SAXException{
      URL url2 = new URL(urlString2);
    
    HttpURLConnection conn2 = (HttpURLConnection) url2.openConnection();
    
    Document geocoderResultDocument2 = null;
    try {
      // open the connection and get results as InputSource.
      conn2.connect();
      InputSource geocoderResultInputSource2 = new InputSource(conn2.getInputStream());

      // read result and parse into XML Document
      geocoderResultDocument2 = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(geocoderResultInputSource2);
    } finally {
      conn2.disconnect();
    }

    // Process the results
    NodeList nodes2 = process(geocoderResultDocument2, XPATH_EXPRESSION2);
    
    // Print results
    String nodeString2="";
    for (int k = 0; k < nodes2.getLength(); k++) {
      nodeString2 = nodes2.item(k).getTextContent();
      System.out.print(nodeString2);
      System.out.print("\n");
    }
  }
  
  public static NodeList process(Document xml, String xPathString) throws IOException {

    NodeList result = null;

    System.out.print("Geocode Processor 1.0\n");

        XPathFactory factory = XPathFactory.newInstance();

    XPath xpath = factory.newXPath();

    try {
      result = (NodeList) xpath.evaluate(xPathString, xml, XPathConstants.NODESET);
    } catch (XPathExpressionException ex) {
          // Deal with it
    }
    return result;
  }
}