package com.sdakak.brandgravity;

import java.util.ArrayList;
import java.util.Iterator;

// Methods to calculate diff for existing stores
public class DiffPerBrand {

  public DiffPerBrand(ArrayList<Brand> allBrands) throws Exception {
    // Read file containing store locations
    FileNames storeFileNames = new FileNames("storesTest");
    FileNames rentalFileNames = new FileNames("diffPerBrand");
    
    // calculate diff for each brand
//    for (Brand currentBrand : allBrands) {
//      String currentBrandName = currentBrand.getBrandName();
//      for (Address store : currentBrand.getBrandStores()) {
//        String rentalName = currentBrandName + "-" + store.getStreetAddress().replace(',', ' ');
//        rentalName = rentalName.replace(' ', '_');
//        Location pseudoLocation = new Location(rentalName, currentBrandName, -100, store);
//      }
//    }

    // print diff per brand
  }

  public static double calculateDiffForExistingStore(Location location, ArrayList<Brand> allBrands,
      FileNames storeFileNames, FileNames rentalFileNames, boolean simulatedRentalFile)
      throws Exception {

    ArrayList<Brand> allBrandsWithLocationRemoved = Util.removeRentalLocation(location, allBrands);
    ArrayList<Location> rentalLocationsWithOneLocationOnly = new ArrayList<Location>();
    rentalLocationsWithOneLocationOnly.add(location);

    BrandDistances brandDistances =
        Combinatorics.calculateAverageDistance(allBrandsWithLocationRemoved, storeFileNames, false);

    LocationAnalysis locationAnalysis;

    // simulatedRentalFile means there is no actual file containing rental Locations.
    if (simulatedRentalFile) {
      locationAnalysis =
          new LocationAnalysis(allBrandsWithLocationRemoved, rentalFileNames, location,
              brandDistances);
    } else {
      locationAnalysis =
          new LocationAnalysis(allBrandsWithLocationRemoved, rentalFileNames, brandDistances);
    }

    locationAnalysis.analyzeLocations(false);
    return locationAnalysis.getDiff(location);

  }

}
