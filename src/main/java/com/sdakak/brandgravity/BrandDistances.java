package com.sdakak.brandgravity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

public class BrandDistances {
  private ArrayList<BrandPair> brandDistances;

  public BrandDistances() {
    this.brandDistances = new ArrayList<BrandPair>();
  }

  public BrandDistances(ArrayList<BrandPair> brandDistances) {
    this.brandDistances = brandDistances;
  }

  public void add(BrandPair currentBrandPair) {
    brandDistances.add(currentBrandPair);
  }

  // public void add(String brand1, String brand2, double distance) {
  // brandDistances.add(new BrandPair(brand1, brand2, distance));
  // }

  public ArrayList<BrandPair> getBrandDistances() {
    return brandDistances;
  }

  // get the top 3 brands that are closest
  public ArrayList<BrandPair> getClosestBrands() {
    return getClosestBrands(3);
  }

  public ArrayList<BrandPair> getClosestBrands(int numBrands) {
    Collections.sort(brandDistances);
    ArrayList<BrandPair> result = extractBrandPairs(numBrands);
    return result;
  }

  // get the top 3 brands that are closest to currentBrand
  public ArrayList<BrandPair> getClosestBrands(String currentBrand) {
    return getClosestBrands(currentBrand, 3);
  }

  public ArrayList<BrandPair> getClosestBrands(String currentBrand, int numBrands) {
    Collections.sort(brandDistances);
    ArrayList<BrandPair> result = extractBrandPairs(currentBrand, numBrands);
    return result;
  }

  private BrandPair findBrandPair(String brand1, String brand2) {
    BrandPair compareBrandPair = new BrandPair(brand1, brand2);

    for (BrandPair currentResult : brandDistances) {
      if (currentResult.equals(compareBrandPair))
        return currentResult;
    }
    return null;
  }

  public String getDetailedStats(String brand1) {
    String result = "";

    for (BrandPair currentResult : brandDistances) {
      if (currentResult.getBrand1().equalsIgnoreCase(brand1)
          || currentResult.getBrand2().equalsIgnoreCase(brand1))
        result += currentResult.getDetailedStats() + "\n";
    }
    return result;
  }

  // get the average distance in miles between two brands
  public Double getDistance(String brand1, String brand2) {
    return findBrandPair(brand1, brand2).getStats().getMean();

    // return Util.prettyDouble(avgDistance);
  }

  public String getDetailedStats(String brand1, String brand2) {
    return findBrandPair(brand1, brand2).getDetailedStats();

  }

  // get the top 3 brands that are furthest
  public ArrayList<BrandPair> getFurthestBrands() {
    return getFurthestBrands(3);
  }

  public ArrayList<BrandPair> getFurthestBrands(int numBrands) {
    Collections.sort(brandDistances, Collections.reverseOrder());
    ArrayList<BrandPair> result = extractBrandPairs(numBrands);
    return result;
  }

  // get the top 3 brands that are furthest from currentBrand
  public ArrayList<BrandPair> getFurthestBrands(String currentBrand) {
    return getFurthestBrands(currentBrand, 3);
  }

  public ArrayList<BrandPair> getFurthestBrands(String currentBrand, int numBrands) {
    Collections.sort(brandDistances, Collections.reverseOrder());
    ArrayList<BrandPair> result = extractBrandPairs(currentBrand, numBrands);
    return result;
  }

  public void remove(BrandPair currentBrandPair) {
    brandDistances.remove(currentBrandPair);
  }

  public void setBrandDistances(ArrayList<BrandPair> brandDistances) {
    this.brandDistances = brandDistances;
  }

  @Override
  public String toString() {
    String result = "";
    for (BrandPair currentBrandPair : brandDistances)
      result += currentBrandPair + "\n";

    return result;
  }

  // Extract the first x BrandPairs
  private ArrayList<BrandPair> extractBrandPairs(int numBrands) {
    ArrayList<BrandPair> result = new ArrayList<BrandPair>();
    for (int i = 0; i < numBrands; i++) {
      result.add(brandDistances.get(i));
    }
    return result;
  }

  // Extract the first x BrandPairs that have the currentBrand. TODO Move to
  // another Filter Class?
  private ArrayList<BrandPair> extractBrandPairs(String currentBrand, int numBrands) {
    // this hack is as ugly as a blobfish with slutty make-up
    BrandPair compareBrandPair = new BrandPair(currentBrand, currentBrand);

    ArrayList<BrandPair> result = new ArrayList<BrandPair>();
    int count = 0;
    Iterator<BrandPair> it = brandDistances.iterator();

    while (it.hasNext() && count < numBrands) {
      BrandPair current = it.next();
      if (compareBrandPair.equals(current)) {
        result.add(current);
        count++;
      }

    }
    return result;
  }

}
