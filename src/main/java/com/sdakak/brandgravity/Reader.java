package com.sdakak.brandgravity;

import java.io.FileReader;
import java.util.ArrayList;

import au.com.bytecode.opencsv.CSVReader;

import com.javadocmd.simplelatlng.LatLng;

// Parse the input file to create a list of all brands and their stores
// author: sdakak

public class Reader {

  // Unused. Can be removed
  // public static BrandDistances readAvgDistanceFile(String outfile) throws Exception {
  // BrandDistances brandDistances = new BrandDistances();
  //
  // CSVReader csvReader = new CSVReader(new FileReader(outfile));
  // String[] nextLine;
  //
  // while ((nextLine = csvReader.readNext()) != null) {
  // String firstBrandName = nextLine[FileFormat.getResultsFirstBrandNameColumn()];
  // String secondBrandName = nextLine[FileFormat.getResultsSecondBrandNameColumn()];
  // double distance = Double.parseDouble(nextLine[FileFormat.getResultsAverageDistanceColumn()]);
  //
  // BrandPair brandPair = new BrandPair(firstBrandName, secondBrandName, distance);
  // brandDistances.add(brandPair);
  //
  // }
  //
  // csvReader.close();
  // return brandDistances;
  // }

  public static ArrayList<Brand> readInputFile(String filename) throws Exception {
    ArrayList<Brand> allBrands = new ArrayList<Brand>();
    // System.out.println("Message1: " + System.getProperty("user.dir"));
    // System.out.println("Message2: " + new File(".").getAbsolutePath());

    CSVReader csvReader = new CSVReader(new FileReader(filename));
    String[] nextLine;
    String lastSeenBrandName = ""; // to keep track of brand being currently
                                   // processed
    String currentBrandName; // Brand label extracted from current line

    Brand currentBrand = new Brand("NOT_SET_YET");
    Address currentAddress;

    while ((nextLine = csvReader.readNext()) != null) {
      currentBrandName = nextLine[FileFormat.getInputBrandNameColumn()];

      if (lastSeenBrandName == "") // special starting case
      {
        lastSeenBrandName = currentBrandName;
        currentBrand.setBrandName(currentBrandName);
      }

      if (!currentBrandName.equals(lastSeenBrandName)) // new brand begins
      {
        allBrands.add(currentBrand);
        currentBrand = new Brand(currentBrandName);
        lastSeenBrandName = currentBrandName;
      }

      LatLng latLong = Geo.stringToLatLng(nextLine[FileFormat.getInputLatLongColumn()]);
      String streetAddress = nextLine[FileFormat.getInputStreetAddressColumn()];
      String city = nextLine[FileFormat.getInputCityColumn()];
      String state = nextLine[FileFormat.getInputStateColumn()];
      String zip = nextLine[FileFormat.getInputZipColumn()];

      currentAddress = new Address(latLong, streetAddress, city, state, zip);

      currentBrand.add(currentAddress);

    }
    allBrands.add(currentBrand);

    csvReader.close();
    return allBrands;
  }

  public static ArrayList<Location> readRentalLocationFile(String filename) throws Exception {
    ArrayList<Location> rentalLocations = new ArrayList<Location>();

    CSVReader csvReader = new CSVReader(new FileReader(filename));
    String[] nextLine;

    while ((nextLine = csvReader.readNext()) != null) {

      String rentalName = nextLine[FileFormat.getRentalName()];
      Double rent = Double.parseDouble(nextLine[FileFormat.getRentalRent()]);
      String brandName = nextLine[FileFormat.getRentalBrandNameColumn()];
      LatLng latLong = Geo.stringToLatLng(nextLine[FileFormat.getRentalLatLongColumn()]);
      String streetAddress = nextLine[FileFormat.getRentalStreetAddressColumn()];
      String city = nextLine[FileFormat.getRentalCityColumn()];
      String state = nextLine[FileFormat.getRentalStateColumn()];
      String zip = nextLine[FileFormat.getRentalZipColumn()];

      Address rentalAddress = new Address(latLong, streetAddress, city, state, zip);

      Location location = new Location(rentalName, brandName, rent, rentalAddress);
      rentalLocations.add(location);
    }

    csvReader.close();
    return rentalLocations;

  }
  
  public static ArrayList<Store> readRentalLocationClosestPairFile(String filename) throws Exception {
	    ArrayList<Store> closestStores = new ArrayList<Store>();

    CSVReader csvReader = new CSVReader(new FileReader(filename));
    String[] nextLine;

    while ((nextLine = csvReader.readNext()) != null) {

      String brandName = nextLine[FileFormat.getClosestPairSecondBrandNameColumn()];
      String location = nextLine[FileFormat.getClosestPairSecondBrandLatLongColumn()];

      Store store = new Store(brandName, location);
      closestStores.add(store);
    }

    csvReader.close();
    return closestStores;
  }
}
