package com.sdakak.brandgravity;

import java.io.FileReader;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;

import au.com.bytecode.opencsv.CSVReader;

// Utility methods for BrandGravity.
// author: sdakak
public class Util {

  public static ArrayList<Brand> copyAllBrands(ArrayList<Brand> allBrands) {
    ArrayList<Brand> copyAllBrands = new ArrayList<Brand>();

    for (Brand currentBrand : allBrands) {
      copyAllBrands.add(currentBrand.clone());
    }
    return copyAllBrands;
  }

  public static double prettyDouble(Double num) {
    DecimalFormat decimalFormatter = new DecimalFormat("#,###,###,##0.00");
    double dd2dec = new Double(decimalFormatter.format(num)).doubleValue();
    return dd2dec;
  }

  // Print a CSV file
  public static void printCSVFile(String filename) throws Exception {
    CSVReader reader = new CSVReader(new FileReader(filename));
    String[] nextLine;
    while ((nextLine = reader.readNext()) != null) {
      // nextLine[] is an array of values from the line
      for (String field : nextLine) {
        System.out.println(field);
      }
      System.out.println();
    }
    reader.close();
  }

  // Print an ArrayList of strings
  public static void printList(ArrayList<String> list) {
    for (String current : list) {
      System.out.println(current);
    }
  }

  // Print an array of strings
  public static void printList(String[] list) {
    for (String current : list) {
      System.out.println(current);
    }
  }

  // remove a brand
  @SuppressWarnings("unused")
  private static void removeBrand(String removeBrandName, ArrayList<Brand> allBrands) {
    Iterator<Brand> it = allBrands.iterator();
    while (it.hasNext()) {
      Brand currentBrand = it.next();
      if (currentBrand.getBrandName().equalsIgnoreCase(removeBrandName)) {
        it.remove();
      }
    }

  }
  
  // remove a store
  public static ArrayList<Brand> removeRentalLocation(Location location, ArrayList<Brand> allBrands) {
    ArrayList<Brand> allBrandsWithLocationRemoved = copyAllBrands(allBrands);

    Iterator<Brand> brandIterator = allBrandsWithLocationRemoved.iterator();
    while (brandIterator.hasNext()) {
      Brand currentBrand = brandIterator.next();

      if (currentBrand.getBrandName().equalsIgnoreCase(location.getBrandName())) {
        Iterator<Address> storeIterator = currentBrand.getBrandStores().iterator();

        while (storeIterator.hasNext()) {
          Address currentAddress = storeIterator.next();
          if (currentAddress.equals(location.getAddress())) {
            storeIterator.remove();
            break;
          }
        }

      }
    }

    return allBrandsWithLocationRemoved;

  }
}
