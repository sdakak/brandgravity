package com.sdakak.brandgravity;

import com.javadocmd.simplelatlng.LatLng;
import com.javadocmd.simplelatlng.LatLngTool;
import com.javadocmd.simplelatlng.util.LengthUnit;

// Class for calculating Geographic distances
// author: sdakak
public class Geo {
  public static double getDistance(Address location1, Address location2) {
    LatLng point1 = location1.getCoordinates();
    LatLng point2 = location2.getCoordinates();

    return getDistance(point1, point2);
  }

  public static double getDistance(LatLng point1, LatLng point2) {
    double distanceInMiles = LatLngTool.distance(point1, point2, LengthUnit.MILE);
    return distanceInMiles;
  }

  public static double getDistance(String location1, String location2) {
    return getDistance(stringToLatLng(location1), stringToLatLng(location2));
  }
  
  public static LatLng getPointOnPath(LatLng point1, LatLng point2, double distance) {
	    double heading = LatLngTool.initialBearing(point1, point2);
	    return LatLngTool.travel(point1, heading, distance, LengthUnit.MILE);
  }
  
  public static String getPointOnPathString(LatLng point1, LatLng point2, double distance) {
    LatLng point = getPointOnPath(point1, point2, distance);
    return point.toString().substring(1, point.toString().length()-1);
}

  public static LatLng stringToLatLng(String coordinates) {
    String[] splitString = coordinates.split(",");
    double pointLatitude = Double.parseDouble(splitString[0]);
    double pointLongitude = Double.parseDouble(splitString[1]);
    LatLng point = new LatLng(pointLatitude, pointLongitude);
    return point;
  }

}
