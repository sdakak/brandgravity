package com.sdakak.brandgravity;

import java.util.ArrayList;


public class Location implements Comparable<Location> {
  private String brandName;
  private String rentalName;
  private double rent;
  private Address address;

  private BrandDistances distanceToOtherBrands;
  private BrandDistances diffMatrix;
  
  private ArrayList<Store> closestStores;

  public Location(String rentalName, String brandName, double rent, Address address) {
    this.rentalName = rentalName;
    this.brandName = brandName;
    this.rent = rent;
    this.address = address;
    this.distanceToOtherBrands = new BrandDistances();
    this.diffMatrix = new BrandDistances();
    closestStores = new ArrayList<Store>();
  }
  
	public void setClosestStores(ArrayList<Store> closestStores) {
		this.closestStores = closestStores;
	}
	
	public String findStore(String brandName) {
	
		for (Store currentStore : closestStores) {
			if (currentStore.getBrandName().equalsIgnoreCase(brandName))
				return currentStore.getStringCoordinates();
		}
		return null;
	}

  public int compareTo(Location compareLocation) {
    double compareDiff = compareLocation.getTotalDiff();

    // ascending order
    if (getTotalDiff() > compareDiff)
      return 1;
    else
      return -1;
  }

  @Override
  public boolean equals(Object obj) {
    // 0.003 miles = 5m = 16 ft
    final double DISTANCE_THRESHOLD = 0.003;

    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Location other = (Location) obj;
    if (brandName.equalsIgnoreCase(other.getBrandName())
        && (Geo.getDistance(address, other.getAddress()) < DISTANCE_THRESHOLD)) {
      return true;
    } else
      return false;

  }

  public Address getAddress() {
    return address;
  }

  public String getBrandName() {
    return brandName;
  }

  public BrandDistances getDiffMatrix() {
    return diffMatrix;
  }

  public BrandDistances getDistanceToOtherBrands() {
    return distanceToOtherBrands;
  }

  public double getRent() {
    return rent;
  }

  public String getRentalName() {
    return rentalName;
  }

  public double getTotalDiff() {
    double totalDiff = 0;
    ArrayList<BrandPair> brandDistances = diffMatrix.getBrandDistances();
    for (BrandPair currentBrandPair : brandDistances) {
      totalDiff += currentBrandPair.getStats().getMean();
    }
    return Util.prettyDouble(totalDiff);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((address == null) ? 0 : address.hashCode());
    result = prime * result + ((brandName == null) ? 0 : brandName.hashCode());
    result = prime * result + ((diffMatrix == null) ? 0 : diffMatrix.hashCode());
    result =
        prime * result + ((distanceToOtherBrands == null) ? 0 : distanceToOtherBrands.hashCode());
    long temp;
    temp = Double.doubleToLongBits(rent);
    result = prime * result + (int) (temp ^ (temp >>> 32));
    result = prime * result + ((rentalName == null) ? 0 : rentalName.hashCode());
    temp = Double.doubleToLongBits(getTotalDiff());
    result = prime * result + (int) (temp ^ (temp >>> 32));
    return result;
  }

  public void setDiffMatrix(BrandDistances diffMatrix) {
    this.diffMatrix = diffMatrix;
  }

  public void setDistanceToOtherBrands(BrandDistances distancesToOtherBrands) {
    this.distanceToOtherBrands = distancesToOtherBrands;
  }

  @Override
  public String toString() {
    return "Location [brandName=" + brandName + ", rentalName=" + rentalName + ", rent=" + rent
        + ", address=" + address + ", distanceToOtherBrands=" + distanceToOtherBrands
        + ", diffMatrix=" + diffMatrix + ", totalDiff=" + Util.prettyDouble(getTotalDiff()) + "]";
  }

}
