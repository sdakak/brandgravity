package com.sdakak.brandgravity;

import java.net.URL;

// Lightweight class that encapsulates all the fileNames used
// author: sdakak
public class FileNames {
  // private static final String EXTENSION = ".csv";
  private String inputFile;
  private String permutationFile;
  private String closestPairFile;
  private String avgDistanceFile;

  public FileNames() {
    this("stores");
  }

  public FileNames(String inputFile) {
    URL urlPath = this.getClass().getResource("/" + inputFile + FileFormat.getExtension());
    String stringPath = urlPath.getFile();

    // stringPath = stringPath.replace("!", "");
    // stringPath = stringPath.replace("", "");
    // System.out.println("### " + stringPath);

    // locationFile = fileDirectory + inputFile;
    this.inputFile = stringPath;
    this.permutationFile = inputFile + "Permutation";
    this.closestPairFile = inputFile + "ClosestPair";
    this.avgDistanceFile = inputFile + "AverageDistance";

    // System.out.println("### locationFile: " + locationFile);
    // System.out.println("### permutationFile: " + permutationFile);
    // System.out.println("### closentPairFile: " + closestPairFile);
    // System.out.println("###: avgDistanceFile: " + avgDistanceFile);

    // addExtensionToFileNames();
  }

  public String getAvgDistanceFile() {
    return avgDistanceFile;
  }

  public String getClosestPairFile() {
    return closestPairFile;
  }

  public String getInputFile() {
    return inputFile;
  }

  public String getPermutationFile() {
    return permutationFile;
  }

  @SuppressWarnings("unused")
  private void addExtensionToFileNames() {
    // permutationFile += EXTENSION;
    // closestPairFile += EXTENSION;
    // avgDistanceFile += EXTENSION;
  }

}
