package com.sdakak.brandgravity;

/*
 * To change this template, choose Tools | Templates
 * 
 * and open the template in the editor.
 */
// package locationfetcher;

import java.io.FileWriter;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import au.com.bytecode.opencsv.CSVWriter;

/**
 * 
 * @author Rohit Kumar
 */
public class StoreFetcher {

  /**
   * @param args the command line arguments
   */

  private static final String GEOCODE_REQUEST_PREFIX_RADAR =
      "https://maps.googleapis.com/maps/api/place/radarsearch/xml";
  private static final String GEOCODE_REQUEST_PREFIX_DETAILS =
      "https://maps.googleapis.com/maps/api/place/details/xml?reference=";
  private static final String GEOCODE_REQUEST_QUERY_DETAILS =
      "&sensor=true&key=AIzaSyDzIYwTrjbv2uG0k5paPn1-6j1mB4Ho_TA";
  private static final String GEOCODE_REQUEST_QUERY[] =
      {
          "?location=39.949753,-75.163178&radius=804672&name=McDonalds&types=food&sensor=false&key=AIzaSyDzIYwTrjbv2uG0k5paPn1-6j1mB4Ho_TA",
          "?location=39.949753,-75.163178&radius=804672&name=KFC&types=food&sensor=false&key=AIzaSyDzIYwTrjbv2uG0k5paPn1-6j1mB4Ho_TA",
          "?location=39.949753,-75.163178&radius=804672&name=CVS&types=pharmacy&sensor=false&key=AIzaSyDzIYwTrjbv2uG0k5paPn1-6j1mB4Ho_TA",
          "?location=39.949753,-75.163178&radius=804672&name=Subway&types=food&sensor=false&key=AIzaSyDzIYwTrjbv2uG0k5paPn1-6j1mB4Ho_TA",
          "?location=39.949753,-75.163178&radius=804672&name=StarBucks&types=food&sensor=false&key=AIzaSyDzIYwTrjbv2uG0k5paPn1-6j1mB4Ho_TA",
          "?location=39.949753,-75.163178&radius=804672&name=Walmart&types=grocery_or_supermarket&sensor=false&key=AIzaSyDzIYwTrjbv2uG0k5paPn1-6j1mB4Ho_TA",
          "?location=39.949753,-75.163178&radius=804672&name=Wendys&types=food&sensor=false&key=AIzaSyDzIYwTrjbv2uG0k5paPn1-6j1mB4Ho_TA",
          "?location=39.949753,-75.163178&radius=804672&name=Arbys&types=food&sensor=false&key=AIzaSyDzIYwTrjbv2uG0k5paPn1-6j1mB4Ho_TA",
          "?location=39.949753,-75.163178&radius=804672&name=Applebees&types=food&sensor=false&key=AIzaSyDzIYwTrjbv2uG0k5paPn1-6j1mB4Ho_TA",
          "?location=39.949753,-75.163178&radius=804672&name=Dominos&types=food&sensor=false&key=AIzaSyDzIYwTrjbv2uG0k5paPn1-6j1mB4Ho_TA"};

  public String _xpath = null;
  public Document _xml = null;

  String nodeString_name[] = {"McDonald's", "KFC", "CVS", "Subway", "StarBucks", "Walmart",
      "Wendys", "Arbys", "AppleBees", "Domino's"};

  private static String XPATH_EXPRESSION_REF = "/PlaceSearchResponse//reference/text()";
  private static String XPATH_EXPRESSION_NAME = "/PlaceDetailsResponse//name/text()";
  private static String XPATH_EXPRESSION_LNG =
      "/PlaceDetailsResponse//geometry/location/lng/text()";
  private static String XPATH_EXPRESSION_LAT =
      "/PlaceDetailsResponse//geometry/location/lat/text()";
  private static String XPATH_EXPRESSION_VIC = "/PlaceDetailsResponse//vicinity/text()";
  // private static String XPATH_EXPRESSION_ZIP =
  // "/PlaceDetailsResponse//address_component[6]/long_name/text()";
  private static String XPATH_EXPRESSION_ZIP =
      "/PlaceDetailsResponse/result/address_component[type='postal_code']/long_name/text()";
  private static String XPATH_EXPRESSION_STATE =
      "/PlaceDetailsResponse/result/address_component[type='administrative_area_level_1']/long_name/text()";
  private static String XPATH_EXPRESSION_CITY =
      "/PlaceDetailsResponse/result/address_component[type='locality']/long_name/text()";

  CSVWriter writer;

  public StoreFetcher() throws Exception {
    // TODO code application logic here
    String inputQuery, resultXml, urlString;
    writer = new CSVWriter(new FileWriter("yourfile.csv"), ',');

    ByteBuffer resultBytes = null;

    for (int i = 0; i < 10; i++) {
      urlString = GEOCODE_REQUEST_PREFIX_RADAR + GEOCODE_REQUEST_QUERY[i];
      // System.out.println(urlString);
      brandResults(urlString, nodeString_name[i]);
      // System.out.println(i+"BRAND DONE");
    }

    writer.close();
  }

  public static NodeList process(Document xml, String xPathString) throws IOException {

    NodeList result = null;

    // System.out.print("Geocode Processor 1.0\n");

    XPathFactory factory = XPathFactory.newInstance();

    XPath xpath = factory.newXPath();

    try {
      result = (NodeList) xpath.evaluate(xPathString, xml, XPathConstants.NODESET);
    } catch (XPathExpressionException ex) {
      // Deal with it
      ex.printStackTrace();
    }
    return result;
  }

  public void brandResults(String urlString, String name) throws IOException, URISyntaxException,
      ParserConfigurationException, SAXException {
    String urlString2;
    // System.out.println("in BRAND RESULTS");
    // Convert the string to a URL so we can parse it
    URL url = new URL(urlString);

    HttpURLConnection conn = (HttpURLConnection) url.openConnection();

    Document geocoderResultDocument = null;
    try {
      // open the connection and get results as InputSource.
      conn.connect();
      InputSource geocoderResultInputSource = new InputSource(conn.getInputStream());

      // read result and parse into XML Document
      geocoderResultDocument =
          DocumentBuilderFactory.newInstance().newDocumentBuilder()
              .parse(geocoderResultInputSource);
    } finally {
      conn.disconnect();
    }

    // Process the results
    NodeList nodes = process(geocoderResultDocument, XPATH_EXPRESSION_REF);
    List<String> nodeString = new ArrayList<String>();
    // Print results
    int i;
    int c = nodes.getLength();
    for (i = 0; i < nodes.getLength(); i++) {
      nodeString.add(nodes.item(i).getTextContent());
      // System.out.print(nodeString.get(i));
      // System.out.print("\n");
    }

    for (int j = 0; j < 10; j++) {
      // System.out.println("ref="+nodeString.get(j));
      urlString2 =
          GEOCODE_REQUEST_PREFIX_DETAILS + URLEncoder.encode(nodeString.get(j), "UTF-8")
              + GEOCODE_REQUEST_QUERY_DETAILS;
      // System.out.println(urlString2);
      detailsResult(urlString2, name);
    }
  }

  public void detailsResult(String urlString2, String name) throws IOException, URISyntaxException,
      ParserConfigurationException, SAXException {

    URL url2 = new URL(urlString2);

    HttpURLConnection conn2 = (HttpURLConnection) url2.openConnection();

    Document geocoderResultDocument2 = null;
    try {
      // open the connection and get results as InputSource.
      conn2.connect();
      InputSource geocoderResultInputSource2 = new InputSource(conn2.getInputStream());

      // read result and parse into XML Document
      geocoderResultDocument2 =
          DocumentBuilderFactory.newInstance().newDocumentBuilder()
              .parse(geocoderResultInputSource2);
    } finally {
      conn2.disconnect();
    }

    // Process the results
    NodeList nodes_vicinity = process(geocoderResultDocument2, XPATH_EXPRESSION_VIC);
    NodeList nodes_lat = process(geocoderResultDocument2, XPATH_EXPRESSION_LAT);
    NodeList nodes_lng = process(geocoderResultDocument2, XPATH_EXPRESSION_LNG);
    NodeList nodes_zip = process(geocoderResultDocument2, XPATH_EXPRESSION_ZIP);
    NodeList nodes_city = process(geocoderResultDocument2, XPATH_EXPRESSION_CITY);
    NodeList nodes_state = process(geocoderResultDocument2, XPATH_EXPRESSION_STATE);

    // Print results

    String nodeString_vicinity = "";
    String nodeString_lat = "";
    String nodeString_lng = "";
    String nodeString_zip = "";
    String nodeString_city = "";
    String nodeString_state = "";

    // nodeString_name = nodes_name.item(0).getTextContent();
    nodeString_vicinity = nodes_vicinity.item(0).getTextContent();
    nodeString_lat = nodes_lat.item(0).getTextContent();
    nodeString_lng = nodes_lng.item(0).getTextContent();
    nodeString_city = nodes_city.item(0).getTextContent();
    nodeString_state = nodes_state.item(0).getTextContent();
    nodeString_zip = nodes_zip.item(0) == null ? "null" : nodes_zip.item(0).getTextContent();
    System.out.print(name + " " + nodeString_vicinity + " " + nodeString_city + " "
        + nodeString_state + " " + nodeString_zip + " " + nodeString_lat + " " + nodeString_lng);
    System.out.print("\n");

    generateCsvFile(name, nodeString_vicinity, nodeString_zip, nodeString_lat, nodeString_lng,
        nodeString_city, nodeString_state);

  }

  private void generateCsvFile(String name, String address, String zip, String lat, String lng,
      String city, String state) {
    try {
      String[] addressline = {name, lat + "," + lng, address, city, state, zip};
      writer.writeNext(addressline);

    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
