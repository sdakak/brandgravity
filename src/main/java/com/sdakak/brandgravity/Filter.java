package com.sdakak.brandgravity;

import java.util.ArrayList;
import java.util.Iterator;

// Class to filter the brands
// author: sdakak
public class Filter {
  // Do not modify the original list
  public static ArrayList<Brand> cloneAndFilter(ArrayList<Brand> allBrands,
      FilterConditions filterConditions) {
    ArrayList<Brand> clonedBrands = Util.copyAllBrands(allBrands);
    filterBrands(clonedBrands, filterConditions);
    return clonedBrands;
  }

  // Just filters by brandName and State for now. MODIFIES THE INPUT LIST
  public static void filterBrands(ArrayList<Brand> allBrands, FilterConditions filterConditions) {
    // filtering my brandNames first gets rid of lots of Addresses with minimal
    // processing
    ArrayList<String> filterBrandNames = filterConditions.getBrandNames();
    if (filterBrandNames != null && !filterBrandNames.isEmpty()) {
      Iterator<Brand> it = allBrands.iterator();
      while (it.hasNext()) {
        Brand currentBrand = (Brand) it.next();
        boolean matched = false;
        for (String currentFilterBrandName : filterBrandNames) {
          if (currentBrand.getBrandName().equalsIgnoreCase(currentFilterBrandName)) {
            matched = true;
            break;
          }
        }

        if (!matched) {
          it.remove();
        }
      }
    }

    // filtetring by state. Logic to filter by other AddressFields (city, state,
    // etc) should be added here
    ArrayList<String> filterStates = filterConditions.getStates();
    if (filterStates != null && !filterStates.isEmpty()) {
      for (Brand currentBrand : allBrands) {

        ArrayList<Address> currentBrandStores = currentBrand.getBrandStores();
        Iterator<Address> it = currentBrandStores.iterator();
        while (it.hasNext()) {
          Address currentAddress = (Address) it.next();
          boolean matched = false;
          for (String currentFilterState : filterStates) {
            if (currentAddress.getState().equalsIgnoreCase(currentFilterState)) {
              matched = true;
              break;
            }
          }

          if (!matched) {
            it.remove();
          }
        }

      }
    }
  }

  // Return the unique brand names from a list of All Brand Stores
  public static ArrayList<String> getUniqueBrandNames(ArrayList<Brand> allBrands) {
    ArrayList<String> uniqueBrandNames = new ArrayList<String>();

    for (Brand currentBrand : allBrands) {
      uniqueBrandNames.add(currentBrand.getBrandName());
    }
    return uniqueBrandNames;
  }

}
