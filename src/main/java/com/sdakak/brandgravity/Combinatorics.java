package com.sdakak.brandgravity;

import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

import com.javadocmd.simplelatlng.LatLng;

// BusinessLogic to calculate average distances. See README to know more about
// the files it produces in the files/ directory as it works
// author: sdakak
public class Combinatorics {

  // This is the final result. Its set by the calculateAverageDistances()
  // method.

  // Calculate average distance between brands
  public static BrandDistances calculateAverageDistance(ArrayList<Brand> filteredBrands,
      FileNames fileNames, boolean permuteOnlyFirstBrand) throws Exception {
    writePermutations(filteredBrands, fileNames.getPermutationFile(), permuteOnlyFirstBrand);
    writeClosestPairs(fileNames.getPermutationFile(), fileNames.getClosestPairFile());

    return calculateAverageDistances(fileNames.getClosestPairFile(), fileNames.getAvgDistanceFile());

  }

  /*
   * Calculate the average driving distance between Brands
   * 
   * Note: Order of input file must be preserved from writeClosestPairs().
   * 
   * Order of output file: B1, B2, Avg Distance B1, B3, Avg Distance ... B1, B100, Avg Distance B2,
   * B3, Avg Distance...
   */
  private static BrandDistances calculateAverageDistances(String infile, String outfile)
      throws Exception {
    BrandDistances brandDistances = new BrandDistances();

    CSVReader csvReader = new CSVReader(new FileReader(infile));

    // No longer reading results from output file. Still useful to generate
    CSVWriter csvWriter = new CSVWriter(new FileWriter(outfile), ',');

    double currentDistance;
    // double currentDistance, sumDistance = 0;
    // int countPairs = 0;

    String currentBrand, compareTo;
    String currentBrandLastSeen = null, compareToLastSeen = null;
    String[] nextLine;
    BrandPair currentBrandPair = new BrandPair();

    while ((nextLine = csvReader.readNext()) != null) {
      currentBrand = nextLine[FileFormat.getPermutationFirstBrandNameColumn()];
      compareTo = nextLine[FileFormat.getPermutationSecondBrandNameColumn()];
      currentDistance = Double.parseDouble(nextLine[nextLine.length - 1]);

      // we are still dealing with the same brand pair
      if (currentBrand.equals(currentBrandLastSeen) && compareTo.equals(compareToLastSeen)) {
        currentBrandPair.addDistance(currentDistance);
      }
      // a new brand pair has started
      else {

        if (currentBrandLastSeen != null) // except and not the first time
        {
          currentBrandPair.setBrand1(currentBrandLastSeen);
          currentBrandPair.setBrand2(compareToLastSeen);
          brandDistances.add(currentBrandPair);

          String[] avgDistanceLine =
              {currentBrandLastSeen, compareToLastSeen,
                  String.valueOf(currentBrandPair.getStats().getMean())};
          csvWriter.writeNext(avgDistanceLine);

          currentBrandPair = new BrandPair();

        }


        // reset pointers

        currentBrandLastSeen = currentBrand;
        compareToLastSeen = compareTo;
        currentBrandPair.addDistance(currentDistance);
      }

    }
    // write last pair
    // String[] avgDistanceLine =
    // {currentBrandLastSeen, compareToLastSeen, String.valueOf(sumDistance / countPairs)};
    // csvWriter.writeNext(avgDistanceLine);
    // csvWriter.close();
    //
    // brandDistances.add(new BrandPair(currentBrandLastSeen, compareToLastSeen, sumDistance
    // / countPairs));

    currentBrandPair.setBrand1(currentBrandLastSeen);
    currentBrandPair.setBrand2(compareToLastSeen);
    brandDistances.add(currentBrandPair);

    String[] avgDistanceLine =
        {currentBrandLastSeen, compareToLastSeen,
            String.valueOf(currentBrandPair.getStats().getMean())};
    csvWriter.writeNext(avgDistanceLine);

    csvReader.close();
    csvWriter.close();
    return brandDistances;

  }

  /*
   * Given a file containing permutatationPairs and the distances between them, for every store of a
   * brand, select another store of a different brand that is closest to it, and write it to an
   * output file.
   * 
   * Note: If the order of lines in inputfile is not as specified in generatePermutations this won't
   * work
   * 
   * Order of output file: For every location of 1st brand, output closest pair of 2nd brand. Then
   * output closest pair of 3rd brand.
   */
  private static void writeClosestPairs(String infile, String outfile) throws Exception {
    CSVReader csvReader = new CSVReader(new FileReader(infile));
    CSVWriter csvWriter = new CSVWriter(new FileWriter(outfile), ',');

    String currentBrand, compareTo;
    String currentBrandLastSeen = null, compareToLastSeen = null;
    String shortestDistanceLine[] = null;
    String[] nextLine;

    double currentDistance, shortestDistance = Double.MAX_VALUE;

    while ((nextLine = csvReader.readNext()) != null) {
      currentBrand = nextLine[FileFormat.getPermutationFirstBrandLatLongColumn()];
      compareTo = nextLine[FileFormat.getPermutationSecondBrandNameColumn()];

      currentDistance = Double.parseDouble(nextLine[nextLine.length - 1]);

      // if you are still comparing current location of BrandA with other
      // locations of BrandB
      if (currentBrand.equals(currentBrandLastSeen) && compareTo.equals(compareToLastSeen)) {

        if (currentDistance < shortestDistance) {
          shortestDistance = currentDistance;
          shortestDistanceLine = nextLine;
        }
      }
      // new location has started
      else {
        if (shortestDistanceLine != null) // first time logic
        { // write the previous closest pair
          csvWriter.writeNext(shortestDistanceLine);
        }

        // reset pointers
        currentBrandLastSeen = currentBrand;
        compareToLastSeen = compareTo;
        shortestDistanceLine = nextLine;
        shortestDistance = currentDistance;
      }
    }
    // write final pair
    csvWriter.writeNext(shortestDistanceLine);
    csvReader.close();
    csvWriter.close();
  }

  /*
   * Generate permutations so that each location of a particular brand is paired with every other
   * location of all other brands. The order in which permutations are generated is important to
   * preserve:
   * 
   * For each location of 1st brand, generate all pairs with 2nd brand locations. Similarly generate
   * pairs with 3rd brand, 4th brand locations.
   */
  private static void writePermutations(ArrayList<Brand> allBrands, String filename,
      boolean permuteOnlyFirstBrand) throws Exception {
    CSVWriter csvWriter = new CSVWriter(new FileWriter(filename), ',');

    // pick first brand
    for (int itemListIndex = 0; itemListIndex < allBrands.size(); itemListIndex++) {
      // compare first brand with all following brands
      // set itemListCompareTo = itemListIndex + 1 if you don't want to permute
      // the brand with itself
      for (int itemListCompareTo = itemListIndex; itemListCompareTo < allBrands.size(); itemListCompareTo++) {
        // for every location in the first brand
        for (int item = 0; item < allBrands.get(itemListIndex).getBrandStores().size(); item++) {
          // compare it with every location of 2nd brand
          for (int itemCompareTo = 0; itemCompareTo < allBrands.get(itemListCompareTo)
              .getBrandStores().size(); itemCompareTo++) {

            Address base = allBrands.get(itemListIndex).getBrandStores().get(item);
            Address compareTo =
                allBrands.get(itemListCompareTo).getBrandStores().get(itemCompareTo);

            // this prevents the closest distance from being 0 when the same
            // brand is being compared with itself
            if (base == compareTo) {
              continue;
            }

            // will contain permutation pair
            ArrayList<String> currentPair = new ArrayList<String>();

            currentPair.add(allBrands.get(itemListIndex).getBrandName());
            currentPair.add(base.getCoordinates().toString());

            currentPair.add(allBrands.get(itemListCompareTo).getBrandName());
            currentPair.add(compareTo.getCoordinates().toString());

            // parsing out the lat long to get the distance between points
            LatLng coordinate1 = base.getCoordinates();
            LatLng coordinate2 = compareTo.getCoordinates();
            currentPair.add(String.valueOf(Geo.getDistance(coordinate1, coordinate2)));

            // Need a simple array to conveniently write to csv file
            String[] pairs = new String[currentPair.size()];
            currentPair.toArray(pairs);

            csvWriter.writeNext(pairs);
          }
        }
      }
      if (permuteOnlyFirstBrand)
        break;
    }
    csvWriter.close();
  }

}
