/*
 * package com.sdakak.brandgravity;
 * 
 * 
 * To change this template, choose Tools | Templates and open the template in the editor.
 *//**
 *
 * author Sheri
 */
/*
 * 
 * import java.awt.Color; import java.awt.Graphics2D; import java.util.ArrayList; import
 * java.util.HashSet; import java.util.Set; import java.util.logging.Level; import
 * java.util.logging.Logger;
 * 
 * import org.jdesktop.swingx.JXMapViewer; import org.jdesktop.swingx.mapviewer.GeoPosition; import
 * org.jdesktop.swingx.mapviewer.Waypoint; import org.jdesktop.swingx.mapviewer.WaypointPainter;
 * import org.jdesktop.swingx.mapviewer.WaypointRenderer; import
 * org.jdesktop.swingx.painter.CompoundPainter;
 * 
 * import au.com.bytecode.opencsv.CSVReader;
 * 
 * public class BrandSelection extends javax.swing.JFrame {
 *//**
 * Creates new form BrandSelection
 */
/*
 * // Variables declaration - private javax.swing.JButton jAlertButton; private javax.swing.JDialog
 * jAlertDialog; private javax.swing.JComboBox<String> jBrandCombo1; private
 * javax.swing.JComboBox<String> jBrandCombo2; private javax.swing.JButton jCompare; private
 * javax.swing.JButton jImportButton; private javax.swing.JLabel jLabel1; private javax.swing.JLabel
 * jLabel2; private javax.swing.JLabel jLabel3; private javax.swing.JLabel jLabel4; private
 * javax.swing.JLabel jLabel5; private javax.swing.JLabel jLabel6; private javax.swing.JPanel
 * jPanel1; private javax.swing.JPanel jPanel2; private javax.swing.JPanel jPanel3; private
 * javax.swing.JPanel jPanel4; private javax.swing.JPasswordField jPasswordField1; private
 * javax.swing.JTabbedPane jTabbedPane1; private javax.swing.JLabel jValueLabel; private
 * javax.swing.JButton jVillanova; private org.jdesktop.swingx.JXMapKit jXMapKit1; private
 * BusinessLogic businessLogic; private ArrayList<Brand> allBrands;
 * 
 * // End of variables declaration// public BrandSelection(ArrayList<Brand> allBrands, BusinessLogic
 * businessLogic) throws Exception { initComponents();
 * 
 * this.allBrands = allBrands; this.businessLogic = businessLogic; }
 * 
 * @SuppressWarnings("unchecked") // <editor-fold defaultstate="collapsed" //
 * desc="Generated Code">//GEN-BEGIN:initComponents private void initComponents() {
 * 
 * jAlertDialog = new javax.swing.JDialog(); jPanel3 = new javax.swing.JPanel(); jLabel6 = new
 * javax.swing.JLabel(); jAlertButton = new javax.swing.JButton(); jTabbedPane1 = new
 * javax.swing.JTabbedPane(); jPasswordField1 = new javax.swing.JPasswordField(); jPanel1 = new
 * javax.swing.JPanel(); jLabel1 = new javax.swing.JLabel(); jLabel2 = new javax.swing.JLabel();
 * jLabel3 = new javax.swing.JLabel(); jBrandCombo1 = new javax.swing.JComboBox<String>();
 * jBrandCombo2 = new javax.swing.JComboBox<String>(); jImportButton = new javax.swing.JButton();
 * jCompare = new javax.swing.JButton(); jPanel2 = new javax.swing.JPanel(); jLabel4 = new
 * javax.swing.JLabel(); jLabel5 = new javax.swing.JLabel(); jValueLabel = new javax.swing.JLabel();
 * jPanel4 = new javax.swing.JPanel(); jXMapKit1 = new org.jdesktop.swingx.JXMapKit(); jVillanova =
 * new javax.swing.JButton();
 * 
 * jAlertDialog.setTitle("Alert!!");
 * 
 * jLabel6.setText("Please Select 2 Different Brands");
 * 
 * jAlertButton.setText("OK"); jAlertButton.addActionListener(new java.awt.event.ActionListener() {
 * public void actionPerformed(java.awt.event.ActionEvent evt) { jAlertButtonActionPerformed(evt); }
 * });
 * 
 * javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
 * jPanel3.setLayout(jPanel3Layout);
 * jPanel3Layout.setHorizontalGroup(jPanel3Layout.createParallelGroup(
 * javax.swing.GroupLayout.Alignment.LEADING).addGroup( jPanel3Layout .createSequentialGroup()
 * .addGroup( jPanel3Layout .createParallelGroup( javax.swing.GroupLayout.Alignment.LEADING)
 * .addGroup( jPanel3Layout .createSequentialGroup() .addGap(234, 234, 234) .addComponent(jLabel6,
 * javax.swing.GroupLayout.PREFERRED_SIZE, 194, javax.swing.GroupLayout.PREFERRED_SIZE)) .addGroup(
 * jPanel3Layout.createSequentialGroup() .addGap(287, 287, 287).addComponent(jAlertButton)))
 * .addContainerGap(282, Short.MAX_VALUE)));
 * jPanel3Layout.setVerticalGroup(jPanel3Layout.createParallelGroup(
 * javax.swing.GroupLayout.Alignment.LEADING).addGroup( jPanel3Layout .createSequentialGroup()
 * .addContainerGap(149, Short.MAX_VALUE) .addComponent(jLabel6,
 * javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE).addGap(18,
 * 18, 18) .addComponent(jAlertButton).addGap(137, 137, 137)));
 * 
 * javax.swing.GroupLayout jAlertDialogLayout = new javax.swing.GroupLayout(
 * jAlertDialog.getContentPane()); jAlertDialog.getContentPane().setLayout(jAlertDialogLayout);
 * jAlertDialogLayout.setHorizontalGroup(jAlertDialogLayout
 * .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING) .addGroup(
 * javax.swing.GroupLayout.Alignment.TRAILING, jAlertDialogLayout .createSequentialGroup()
 * .addContainerGap() .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE,
 * javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
 * jAlertDialogLayout.setVerticalGroup(jAlertDialogLayout.createParallelGroup(
 * javax.swing.GroupLayout.Alignment.LEADING).addGroup( jAlertDialogLayout .createSequentialGroup()
 * .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE,
 * javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE) .addContainerGap()));
 * 
 * jPasswordField1.setText("jPasswordField1");
 * 
 * setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
 * 
 * jLabel1.setText("     Brand Gravity Analyzer");
 * 
 * jLabel2.setText("Select Brand 1:");
 * 
 * jLabel3.setText("Select Brand 2:");
 * 
 * jBrandCombo1.addActionListener(new java.awt.event.ActionListener() { public void
 * actionPerformed(java.awt.event.ActionEvent evt) { jBrandCombo1ActionPerformed(evt); } });
 * 
 * jImportButton.setText("Import Brands"); jImportButton.addActionListener(new
 * java.awt.event.ActionListener() { public void actionPerformed(java.awt.event.ActionEvent evt) {
 * jImportButtonActionPerformed(evt); } });
 * 
 * jCompare.setText("Compare"); jCompare.addActionListener(new java.awt.event.ActionListener() {
 * public void actionPerformed(java.awt.event.ActionEvent evt) { jCompareActionPerformed(evt); } });
 * 
 * javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
 * jPanel1.setLayout(jPanel1Layout); jPanel1Layout .setHorizontalGroup(jPanel1Layout
 * .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING) .addGroup( jPanel1Layout
 * .createSequentialGroup() .addGap(38, 38, 38) .addComponent(jImportButton) .addGap(28, 28, 28)
 * .addGroup( jPanel1Layout .createParallelGroup( javax.swing.GroupLayout.Alignment.LEADING)
 * .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 179,
 * javax.swing.GroupLayout.PREFERRED_SIZE) .addGroup( jPanel1Layout .createSequentialGroup()
 * .addGap(29, 29, 29) .addGroup( jPanel1Layout .createParallelGroup(
 * javax.swing.GroupLayout.Alignment.LEADING) .addComponent(jLabel2) .addComponent( jLabel3,
 * javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)) .addGap(26,
 * 26, 26) .addGroup( jPanel1Layout .createParallelGroup( javax.swing.GroupLayout.Alignment.LEADING)
 * .addComponent( jBrandCombo1, javax.swing.GroupLayout.PREFERRED_SIZE, 103,
 * javax.swing.GroupLayout.PREFERRED_SIZE) .addComponent( jBrandCombo2,
 * javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)) .addGap(35,
 * 35, 35).addComponent(jCompare))) .addContainerGap(94, Short.MAX_VALUE))); jPanel1Layout
 * .setVerticalGroup(jPanel1Layout .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
 * .addGroup( jPanel1Layout .createSequentialGroup() .addContainerGap() .addComponent(jLabel1,
 * javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE) .addGroup(
 * jPanel1Layout .createParallelGroup( javax.swing.GroupLayout.Alignment.LEADING) .addGroup(
 * jPanel1Layout .createSequentialGroup() .addGroup( jPanel1Layout .createParallelGroup(
 * javax.swing.GroupLayout.Alignment.BASELINE) .addComponent(jLabel2) .addComponent( jBrandCombo1,
 * javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
 * javax.swing.GroupLayout.PREFERRED_SIZE)) .addPreferredGap(
 * javax.swing.LayoutStyle.ComponentPlacement.RELATED) .addGroup( jPanel1Layout
 * .createParallelGroup( javax.swing.GroupLayout.Alignment.BASELINE) .addComponent( jBrandCombo2,
 * javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
 * javax.swing.GroupLayout.PREFERRED_SIZE) .addComponent( jLabel3,
 * javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))) .addGroup(
 * jPanel1Layout.createSequentialGroup() .addGap(13, 13, 13) .addComponent(jImportButton))
 * .addGroup( jPanel1Layout.createSequentialGroup() .addGap(11, 11, 11).addComponent(jCompare)))
 * .addContainerGap(41, Short.MAX_VALUE)));
 * 
 * jLabel4.setText("Average distance (in Miles):");
 * 
 * // jLabel5.setText("The driving dstance in miles:");
 * 
 * jValueLabel.setEnabled(false);
 * 
 * javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
 * jPanel2.setLayout(jPanel2Layout); jPanel2Layout .setHorizontalGroup(jPanel2Layout
 * .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING) .addGroup(
 * javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout .createSequentialGroup() .addGroup(
 * jPanel2Layout .createParallelGroup( javax.swing.GroupLayout.Alignment.TRAILING) .addGroup(
 * jPanel2Layout .createSequentialGroup() .addContainerGap( javax.swing.GroupLayout.DEFAULT_SIZE,
 * Short.MAX_VALUE) .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 163,
 * javax.swing.GroupLayout.PREFERRED_SIZE) .addGap(49, 49, 49)) .addGroup( jPanel2Layout
 * .createSequentialGroup() .addContainerGap() .addComponent(jLabel5) .addPreferredGap(
 * javax.swing.LayoutStyle.ComponentPlacement.UNRELATED) .addComponent(jValueLabel,
 * javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
 * Short.MAX_VALUE))).addContainerGap()));
 * jPanel2Layout.setVerticalGroup(jPanel2Layout.createParallelGroup(
 * javax.swing.GroupLayout.Alignment.LEADING) .addGroup( jPanel2Layout .createSequentialGroup()
 * .addContainerGap() .addComponent(jLabel4) .addPreferredGap(
 * javax.swing.LayoutStyle.ComponentPlacement.RELATED) .addGroup( jPanel2Layout
 * .createParallelGroup( javax.swing.GroupLayout.Alignment.BASELINE) .addComponent(jLabel5)
 * .addComponent(jValueLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 14,
 * javax.swing.GroupLayout.PREFERRED_SIZE)) .addContainerGap(252, Short.MAX_VALUE)));
 * 
 * jXMapKit1 .setDefaultProvider(org.jdesktop.swingx.JXMapKit.DefaultProviders.OpenStreetMaps);
 * 
 * javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
 * jPanel4.setLayout(jPanel4Layout);
 * jPanel4Layout.setHorizontalGroup(jPanel4Layout.createParallelGroup(
 * javax.swing.GroupLayout.Alignment.LEADING).addGroup( jPanel4Layout .createSequentialGroup()
 * .addContainerGap() .addComponent(jXMapKit1, javax.swing.GroupLayout.PREFERRED_SIZE, 502,
 * javax.swing.GroupLayout.PREFERRED_SIZE) .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE,
 * Short.MAX_VALUE))); jPanel4Layout.setVerticalGroup(jPanel4Layout.createParallelGroup(
 * javax.swing.GroupLayout.Alignment.LEADING).addGroup( jPanel4Layout .createSequentialGroup()
 * .addContainerGap() .addComponent(jXMapKit1, javax.swing.GroupLayout.DEFAULT_SIZE,
 * javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE) .addContainerGap()));
 * 
 * jVillanova.setText("Center on VU"); jVillanova.addActionListener(new
 * java.awt.event.ActionListener() { public void actionPerformed(java.awt.event.ActionEvent evt) {
 * jVillanovaActionPerformed(evt); } });
 * 
 * javax.swing.GroupLayout layout = new javax.swing.GroupLayout( getContentPane());
 * getContentPane().setLayout(layout); layout.setHorizontalGroup(layout
 * .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING) .addGroup( layout
 * .createSequentialGroup() .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE,
 * javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE) .addPreferredGap(
 * javax.swing.LayoutStyle.ComponentPlacement.RELATED) .addComponent(jVillanova,
 * javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE).addGap(18,
 * 18, 18)) .addGroup( javax.swing.GroupLayout.Alignment.TRAILING, layout .createSequentialGroup()
 * .addContainerGap() .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE,
 * javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE) .addPreferredGap(
 * javax.swing.LayoutStyle.ComponentPlacement.RELATED) .addComponent(jPanel2,
 * javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
 * .addContainerGap())); layout.setVerticalGroup(layout.createParallelGroup(
 * javax.swing.GroupLayout.Alignment.LEADING) .addGroup( layout .createSequentialGroup() .addGroup(
 * layout .createParallelGroup( javax.swing.GroupLayout.Alignment.LEADING) .addComponent(jPanel1,
 * javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
 * javax.swing.GroupLayout.PREFERRED_SIZE) .addGroup(
 * layout.createSequentialGroup().addContainerGap() .addComponent(jVillanova))) .addPreferredGap(
 * javax.swing.LayoutStyle.ComponentPlacement.RELATED) .addGroup( layout .createParallelGroup(
 * javax.swing.GroupLayout.Alignment.LEADING) .addComponent(jPanel2,
 * javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
 * .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE,
 * javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)).addContainerGap()));
 * 
 * pack(); }// </editor-fold>//GEN-END:initComponents
 * 
 * private void jAlertButtonActionPerformed(java.awt.event.ActionEvent evt) { // TODO add your
 * handling code here: jAlertDialog.dispose(); }
 * 
 * private void jBrandCombo1ActionPerformed(java.awt.event.ActionEvent evt) { // TODO add your
 * handling code here: }
 * 
 * private void jVillanovaActionPerformed(java.awt.event.ActionEvent evt) {
 * 
 * positionCenter(); }
 * 
 * private void jImportButtonActionPerformed(java.awt.event.ActionEvent evt) { try {
 * 
 * PopulateCombo(); } catch (Exception ex) {
 * Logger.getLogger(BrandSelection.class.getName()).log(Level.SEVERE, null, ex); }
 * 
 * }
 * 
 * private void jCompareActionPerformed(java.awt.event.ActionEvent evt) {
 * 
 * jValueLabel.setEnabled(true); if (jBrandCombo1.getSelectedIndex() ==
 * jBrandCombo2.getSelectedIndex()) {
 * 
 * jAlertDialog.setSize(700, 130); jAlertDialog.setVisible(true);
 * jLabel6.setText("Please select 2 different brands");
 * 
 * } else {
 * 
 * positionCenter(); plot(); correlation(); } }
 * 
 * public void correlation() { printing corelation value String combo1 = (String)
 * jBrandCombo1.getSelectedItem(); String combo2 = (String) jBrandCombo2.getSelectedItem();
 * 
 * try { Double valdouble = businessLogic.getDistance(combo1, combo2); String valstring = new
 * Double(valdouble).toString(); jValueLabel.setEnabled(true); jValueLabel.setText(valstring); }
 * catch (Exception ex) { Logger.getLogger(BrandSelection.class.getName()).log(Level.SEVERE, null,
 * ex); } }
 * 
 * public void plot() { Set<Waypoint> waypoints = new HashSet<Waypoint>(); Set<Waypoint> waypoints1
 * = new HashSet<Waypoint>(); String selection1 = (String) jBrandCombo1.getSelectedItem(); String
 * selection2 = (String) jBrandCombo2.getSelectedItem(); CSVReader reader = null; try {
 * 
 * ArrayList<String> plotBrands = new ArrayList<String>(); plotBrands.add(selection1);
 * plotBrands.add(selection2);
 * 
 * FilterConditions filterConditions = new FilterConditions(plotBrands); ArrayList<Brand>
 * filteredBrands = Filter.cloneAndFilter(allBrands, filterConditions);
 * 
 * latlong(filteredBrands, selection1, waypoints1, waypoints);
 * 
 * } catch (Exception ex) { Logger.getLogger(BrandSelection.class.getName()).log(Level.SEVERE, null,
 * ex); }
 * 
 * painting(waypoints, waypoints1); }
 * 
 * public void PopulateCombo() throws Exception {
 * 
 * ArrayList<String> uniqueBrandNames = Filter.getUniqueBrandNames(allBrands);
 * 
 * for (String current : uniqueBrandNames) { jBrandCombo1.addItem(current);
 * jBrandCombo2.addItem(current);
 * 
 * } }
 * 
 * public void positionCenter() { // positionining map to villanova jXMapKit1.setAddressLocation(new
 * GeoPosition(40.0377, -75.3376));
 * 
 * jXMapKit1.getCenterPosition(); }
 * 
 * public void painting(Set<Waypoint> waypoints, Set<Waypoint> waypoints1) { WaypointPainter
 * painter;
 * 
 * painter = new WaypointPainter(); painter.setWaypoints(waypoints);
 * 
 * painter.setRenderer(new WaypointRenderer<Waypoint>() { public void paintWaypoint(Graphics2D g,
 * JXMapViewer map, Waypoint wp) {
 * 
 * g.setColor(Color.cyan); g.fillRect(-5, -5, 10, 10); g.setColor(Color.BLACK); //
 * g.drawRect(-5,-5,10,10); g.drawRoundRect(-5, -5, 10, 10, 10, 10); // return true;
 * 
 * }
 * 
 * }); WaypointPainter painter1;
 * 
 * painter1 = new WaypointPainter(); painter1.setRenderer(new WaypointRenderer<Waypoint>() { public
 * void paintWaypoint(Graphics2D g, JXMapViewer map, Waypoint wp) {
 * 
 * g.setColor(Color.RED); g.fillRect(-5, -5, 10, 10); g.setColor(Color.BLACK); //
 * g.drawRect(-5,-5,10,10); g.drawRoundRect(-5, -5, 10, 10, 10, 10); // return true;
 * 
 * }
 * 
 * }); painter1.setWaypoints(waypoints1); CompoundPainter cp = new CompoundPainter();
 * 
 * cp.setPainters(painter, painter1); cp.setCacheable(false);
 * 
 * jXMapKit1.getMainMap().setOverlayPainter(cp); }
 * 
 * public void latlong(ArrayList<Brand> allBrands, String selection1, Set<Waypoint> waypoints1,
 * Set<Waypoint> waypoints) throws NumberFormatException {
 * 
 * for (Brand currentBrand : allBrands) { ArrayList<Address> currentBrandStores =
 * currentBrand.getBrandStores(); for (Address currentAddress : currentBrandStores) { Double lat =
 * currentAddress.getCoordinates().getLatitude(); Double longt =
 * currentAddress.getCoordinates().getLongitude();
 * 
 * 
 * if (currentBrand.getBrandName().equalsIgnoreCase(selection1)) {
 * 
 * waypoints.add(new SwingWaypoint) waypoints.add(new Waypoint(41.881944,-87.627778));
 * waypoints1.add(new Waypoint(new GeoPosition(lat, longt))); // adding lat & longitude
 * waypoints1.add(e)
 * 
 * }
 * 
 * // of first brand to // waypoint class else waypoints.add(new Waypoint(lat, longt));// adding
 * lat& long of // seconf brand selected
 * 
 * 
 * }
 * 
 * } } }
 */
