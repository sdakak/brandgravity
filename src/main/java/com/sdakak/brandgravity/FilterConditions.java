package com.sdakak.brandgravity;

import java.util.ArrayList;

// Light weight class that contains filtering selections
// author: sdakak
public class FilterConditions {
  // 0 or null means allow everything
  private ArrayList<String> brandNames;
  private ArrayList<String> cities;
  private ArrayList<String> states;
  private double latitude;
  private double longitude;
  private int zip;
  private double radius; // radius in miles around latLong and zip

  public FilterConditions() {
    this.brandNames = null;
    this.cities = null;
    this.states = null;
    this.latitude = 0;
    this.longitude = 0;
    this.zip = 0;
    this.radius = 0;
  }

  public FilterConditions(ArrayList<String> brandNames) {
    this.brandNames = brandNames;
    this.cities = null;
    this.states = null;
    this.latitude = 0;
    this.longitude = 0;
    this.zip = 0;
    this.radius = 0;
  }

  public FilterConditions(ArrayList<String> brandNames, ArrayList<String> cities,
      ArrayList<String> states, double latitude, double longitude, int zip, double radius) {
    this.brandNames = brandNames;
    this.cities = cities;
    this.states = states;
    this.latitude = latitude;
    this.longitude = longitude;
    this.zip = zip;
    this.radius = radius;
  }

  // getters
  public ArrayList<String> getBrandNames() {
    return brandNames;
  }

  public ArrayList<String> getCities() {
    return cities;
  }

  public double getLatitude() {
    return latitude;
  }

  public double getLongitude() {
    return longitude;
  }

  public double getRadius() {
    return radius;
  }

  public ArrayList<String> getStates() {
    return states;
  }

  public int getZip() {
    return zip;
  }

  // setters
  public void setStates(ArrayList<String> states) {
    this.states = states;
  }
}
