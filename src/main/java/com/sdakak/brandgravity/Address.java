package com.sdakak.brandgravity;

import com.javadocmd.simplelatlng.LatLng;

// Lightweight class to encapsulate each store location or more simply each Address
// author: sdakak
public class Address {

  private LatLng coordinates;
  private String streetAddress;
  private String city;
  private String state;
  private String zip;

  public Address(LatLng coordinates, String streetAddress, String city, String state, String zip) {
    this.coordinates = coordinates;
    this.streetAddress = streetAddress;
    this.city = city;
    this.state = state;
    this.zip = zip;
  }
  
  public Address(String coordinates) {
	    this.coordinates = Geo.stringToLatLng(coordinates);
	    this.streetAddress = "NOT_SET";
	    this.city = "NOT_SET";
	    this.state = "NOT_SET";
	    this.zip = "NOT_SET";
	  }

  @Override
  public Address clone() {
    LatLng clonedLatLong = new LatLng(coordinates.getLatitude(), coordinates.getLongitude());
    // probably don't need to clone immutable strings TODO
    String clonedStreetAddress = streetAddress;
    String clonedCity = city;
    String clonedState = state;
    String clonedZip = zip;
    Address clonedAddress =
        new Address(clonedLatLong, clonedStreetAddress, clonedCity, clonedState, clonedZip);
    return clonedAddress;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Address other = (Address) obj;
    if (coordinates == null) {
      if (other.coordinates != null)
        return false;
    }
    if (Geo.getDistance(this, other) < 0.02) // 0.02 miles = 35 yards = 30 metres
      return true;

    return false;
  }

  public String getCity() {
    return city;
  }

  public LatLng getCoordinates() {
    return coordinates;
  }
  
  public String getStringCoordinates() {
  	return coordinates.toString().substring(1, coordinates.toString().length()-1);
  }

  public String getState() {
    return state;
  }

  public String getStreetAddress() {
    return streetAddress;
  }

  public String getZip() {
    return zip;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public void setCoordinates(LatLng coordinates) {
    this.coordinates = coordinates;
  }

  public void setState(String state) {
    this.state = state;
  }

  public void setStreetAddress(String streetAddress) {
    this.streetAddress = streetAddress;
  }

  public void setZip(String zip) {
    this.zip = zip;
  }

  @Override
  public String toString() {
    return "Location [coordinates=" + coordinates + ", streetAddress=" + streetAddress + ", city="
        + city + ", state=" + state + ", zip=" + zip + "]";

  }
}
