package com.sdakak.brandgravity;

import java.util.ArrayList;
import java.util.Collections;

public class LocationAnalysis {
  ArrayList<Brand> allBrands;
  FileNames fileNames;
  BrandDistances brandDistances;
  ArrayList<Location> rentalLocations;

  public LocationAnalysis(ArrayList<Brand> allBrands, FileNames fileNames,
      BrandDistances brandDistances) throws Exception {
    this.fileNames = fileNames;
    this.allBrands = allBrands;
    this.brandDistances = brandDistances;

    this.rentalLocations = Reader.readRentalLocationFile(fileNames.getInputFile());

    // analyzeLocations();
  }
  
  public LocationAnalysis(ArrayList<Brand> allBrands, FileNames fileNames, Location location,
      BrandDistances brandDistances) throws Exception {
    this.fileNames = fileNames;
    this.allBrands = allBrands;
    this.brandDistances = brandDistances;

    this.rentalLocations = new ArrayList<Location>();
    rentalLocations.add(location);

    // analyzeLocations();
  }

  private void prettyPrint(ArrayList<Location> rentalLocations) {
    Collections.sort(rentalLocations);

    for (int i = 0; i < rentalLocations.size(); i++) {
      Location currentLocation = rentalLocations.get(i);
      System.out.println((i + 1) + ". " + currentLocation.getRentalName() + ". Diff: " + currentLocation.getTotalDiff());
      System.out.println("Prospective Brand Name: " + currentLocation.getBrandName());
      System.out.println("Rent: " + currentLocation.getRent());
      
      System.out.println("High match due to these brands: ");
      ArrayList<BrandPair> closestBrands = currentLocation.getDiffMatrix().getClosestBrands();
      for (BrandPair currentBrandPair : closestBrands) {
      	System.out.println(currentBrandPair.getBrand2() + ", " + currentBrandPair.getMean());
      }
//      for (BrandPair brandPair : closestBrands) {
//    	  System.out.println("Avg distance world: " + brandPair.getBrand1() + " -- " + brandPair.getBrand2() + " : " + brandDistances.getDistance(brandPair.getBrand1(), brandPair.getBrand2()));
//      }
      
      System.out.println("Low match due to these brands: ");
      ArrayList<BrandPair> furthestBrands = currentLocation.getDiffMatrix().getFurthestBrands();
      for (BrandPair currentBrandPair : furthestBrands) {
      	System.out.println(currentBrandPair.getBrand2() + ", " + currentBrandPair.getMean());
      }
//      for (BrandPair brandPair : furthestBrands) {
//    	  System.out.println("Avg distance world: " + brandPair.getBrand1() + " -- " + brandPair.getBrand2() + " : " + brandDistances.getDistance(brandPair.getBrand1(), brandPair.getBrand2()));
//      }
      
      System.out.println();
    }

  }
  
  public double getDiff (Location targetLocation) {
    for (Location currentLocation : rentalLocations) {
      if (currentLocation.equals(targetLocation)) {
        return currentLocation.getTotalDiff();
      }
    }
    return -1.0;
  }

  public void analyzeLocations(boolean prettyPrintOn) throws Exception {
    calculateRentalLocationDistances(rentalLocations);
    calculateRentalLocationDiff(rentalLocations, brandDistances);
    if (prettyPrintOn) {
      prettyPrint(rentalLocations);
    }
  }

  private void calculateRentalLocationDiff(ArrayList<Location> rentalLocations,
      BrandDistances brandDistances) throws Exception {
    for (Location rentalLocation : rentalLocations) {
      ArrayList<String> uniqueBrandNames = Filter.getUniqueBrandNames(allBrands);
      String rentalLocationBrand = rentalLocation.getBrandName();

      for (String brandName : uniqueBrandNames) {
        double worldDistance = brandDistances.getDistance(rentalLocationBrand, brandName);
        double rentalDistance =
            rentalLocation.getDistanceToOtherBrands().getDistance(rentalLocationBrand, brandName);
        double diff = Math.abs(worldDistance - rentalDistance);

        BrandPair currentBrandPair = new BrandPair(rentalLocationBrand, brandName);
        currentBrandPair.addDistance(diff);
        rentalLocation.getDiffMatrix().add(currentBrandPair);
      }

    }
  }

  private void calculateRentalLocationDistances(ArrayList<Location> rentalLocations)
      throws Exception {
    for (Location rentalLocation : rentalLocations) {
      // workaround to make combinatorics work
      Brand pseudoBrand = makePseudoRentalBrand(rentalLocation);
      allBrands.add(0, pseudoBrand);

      BrandDistances rentalToBrandDistances =
          Combinatorics.calculateAverageDistance(allBrands, fileNames, true);

      rentalLocation.setDistanceToOtherBrands(rentalToBrandDistances);

      // return allBrands to original state
      allBrands.remove(0);
      rentalLocation.setClosestStores(Reader.readRentalLocationClosestPairFile(fileNames.getClosestPairFile()));
    }
  }

  private Brand makePseudoRentalBrand(Location rentalLocation) {
    // String pseudoBrandName = rentalLocation.getBrandName() + "_"
    // + rentalLocation.getRentalName();
    String pseudoBrandName = rentalLocation.getBrandName();
    ArrayList<Address> rentalAddresses = new ArrayList<Address>();
    rentalAddresses.add(rentalLocation.getAddress());

    Brand pseudoBrand = new Brand(pseudoBrandName, rentalAddresses);
    return pseudoBrand;
  }

  public double getTotalDiff(Location location) {
    for (Location currentLocation : rentalLocations) {
      if (currentLocation.equals(location)) {
        return Util.prettyDouble(currentLocation.getTotalDiff());
      }
    }
    return -111;
  }

  public double getTotalDiff(String rentalLocationName) {
    for (Location currentLocation : rentalLocations) {
      if (currentLocation.getRentalName().equalsIgnoreCase(rentalLocationName)) {
        return Util.prettyDouble(currentLocation.getTotalDiff());
      }
    }
    return -111;

  }

  public ArrayList<Location> getRentalLocations() {
    return rentalLocations;
  }

}
