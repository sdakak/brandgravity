package com.sdakak.brandgravity;

import java.util.ArrayList;

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

// Class that encapsulates all the stores of a particular brand
// author: sdakak
public class Brand {

  private ArrayList<Address> brandStores;
  private String brandName;
  // private DescriptiveStatistics diffStats;

  public Brand(String brandName) {
    this.brandName = brandName;
    this.brandStores = new ArrayList<Address>();
    // this.diffStats = new DescriptiveStatistics();
  }

  public Brand(String brandName, ArrayList<Address> brandStores) {
    this.brandName = brandName;
    this.brandStores = brandStores;
  }

  public void add(Address currentLocation) {
    brandStores.add(currentLocation);
  }
  
//  public void addDiff(double diff) {
//    diffStats.addValue(diff);
//  }
//  
//  public Double getMean() {
//    return Util.prettyDouble(diffStats.getMean());
//  }
//  
//  public Double getMedian() {
//    return Util.prettyDouble(diffStats.getPercentile(50));
//  }
//
//  public String getDetailedStats() {
//    String result = brandName + " Diff Stats:\n";
//    result += "Mean Diff: " + getMean() + "\n";
//    result += "Median: " + getMedian() + "\n";
//    result +=
//        "Range: " + Util.prettyDouble(diffStats.getMax() - diffStats.getMin()) + " ("
//            + Util.prettyDouble(diffStats.getMin()) + " - " + Util.prettyDouble(diffStats.getMax()) + ")\n";
//    result +=
//        "Interquartile range: "
//            + Util.prettyDouble(diffStats.getPercentile(75) - diffStats.getPercentile(50)) + "\n";
//    result += "Variance: " + Util.prettyDouble(diffStats.getVariance()) + "\n";
//    result += "Standard Deviation: " + Util.prettyDouble(diffStats.getStandardDeviation()) + "\n";
//
//    return result;
//  }

  @Override
  public Brand clone() {
    String clonedBrandName = brandName;
    ArrayList<Address> clonedBrandStores = new ArrayList<Address>();

    for (Address currentAddress : brandStores) {
      clonedBrandStores.add(currentAddress.clone());
    }

    Brand clonedBrand = new Brand(clonedBrandName, clonedBrandStores);
    return clonedBrand;
  }

  public String getBrandName() {
    return brandName;
  }

  public ArrayList<Address> getBrandStores() {
    return brandStores;
  }

  public void remove(Address currentLocation) {
    brandStores.remove(currentLocation);
  }

  public void setBrandName(String brandName) {
    this.brandName = brandName;
  }

  public void setBrandStores(ArrayList<Address> brandStores) {
    this.brandStores = brandStores;
  }

  @Override
  public String toString() {
    String result = brandName + " Stores: \n";
    for (Address currentAddress : brandStores) {
      result += currentAddress + "\n";
    }
    return result;
  }

}
