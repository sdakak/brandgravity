package com.sdakak.brandgravity;

import com.javadocmd.simplelatlng.LatLng;

public class Store {
	String brandName;
	String storeName;
	LatLng coordinates;
	//TODO change this to address and carry address all the way through the algorithm?
	
	public Store (String brandName, String coordinates) {
		this.brandName = brandName;
		this.storeName = "NOT_SET";
		
		if (coordinates.charAt(0) == '(') {
			String coordinatesWithoutBrackets = coordinates.substring(1, coordinates.length()-1);
			this.coordinates = Geo.stringToLatLng(coordinatesWithoutBrackets);
		}
		else {
			this.coordinates = Geo.stringToLatLng(coordinates);
		}
	}
	
	public Store (String brandName, String storeName, String coordinates) {
		this.brandName = brandName;
		this.storeName = storeName;
		this.coordinates = Geo.stringToLatLng(coordinates);
	}

	public String getBrandName() {
		return brandName;
	}

	public String getStoreName() {
		return storeName;
	}

	public LatLng getCoordinates() {
		return coordinates;
	}
	
	public String getStringCoordinates() {
  	return coordinates.toString().substring(1, coordinates.toString().length()-1);
  }

}
