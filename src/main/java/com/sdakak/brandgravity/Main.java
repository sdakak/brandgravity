package com.sdakak.brandgravity;

import java.util.ArrayList;

// The main method exercises all the typical methods in the package
// author: sdakak
public class Main {
  public static void main(String args[]) throws Exception {
    // Read file containing store locations
    FileNames fileNames = new FileNames("storesTest");
    ArrayList<Brand> allBrands = Reader.readInputFile(fileNames.getInputFile());

    // Core business logic to calculate average driving distance of one brand to
    // the other
    BrandDistances brandDistances =
        Combinatorics.calculateAverageDistance(allBrands, fileNames, false);
    
    // What is the average diff of stores in all the brands
    // DiffPerBrand diffPerBrand = new DiffPerBrand(allBrands);

    // Exercising various program functions
    demonstratePrediction(allBrands, brandDistances, true);
    demonstrateCoreFunction(brandDistances, true);
    demonstrateFiltering();
    demonstrateSupportFunctions(allBrands);

    // UI initiation. No longer works :-(
    // java.awt.EventQueue.invokeLater(new Runnable() {
    // public void run()
    // {
    // try
    // {
    // // new BrandSelection(allBrands,brandDistances).setVisible(true);
    // }
    // catch (Exception e)
    // {
    // System.out.println("Something very bad happened");
    // }
    // }
    // });
    // end of uiinitiation
    
  }

  private static void demonstrateCoreFunction(BrandDistances brandDistances, boolean detailedOutput)
      throws Exception {
    // sample code to get overall closest brands
    System.out.println("\n##### CORE FUNCTION \n---Overall 3 closest brands:");
    ArrayList<BrandPair> results = brandDistances.getClosestBrands();
    for (BrandPair result : results) {
      System.out.println(result);
    }

    if (detailedOutput) {

      // sample code to fetch results
      System.out.println("\n---Detailed stats of CVS and KFC: ");
      System.out.println(brandDistances.getDetailedStats("CVS", "KFC"));

      // sample code to get overall furthest brands
      System.out.println("\n----Overall 3 furthest brands:");
      results = brandDistances.getFurthestBrands();
      for (BrandPair result : results) {
        System.out.println(result);
      }

      // sample code to get closest and furthest brands from currentBrand
      System.out.println("\n----3 closest brands to Starbucks:");
      results = brandDistances.getClosestBrands("StarBucks");
      for (BrandPair result : results) {
        System.out.println(result);
      }

      System.out.println("\n----3 furthest brands from Walmart:");
      results = brandDistances.getFurthestBrands("Walmart");
      for (BrandPair result : results) {
        System.out.println(result);
      }

    }

  }

  private static void demonstrateFiltering() throws Exception {
    // testing loading another locatiosn file and filtering by state
    System.out.println("\n##### FILTERING \n---Loading another locations file and "
        + "filtering by state");

    System.out.println("All locations in new file: "); // Returning the LDS

    FileNames fileNames = new FileNames("filterTest");
    ArrayList<Brand> allBrands2 = Reader.readInputFile(fileNames.getInputFile());
    System.out.println(allBrands2);

    // Only alow locations from these states
    ArrayList<String> filterStates = new ArrayList<String>();
    filterStates.add("NY");
    filterStates.add("GA");

    FilterConditions filterConditions = new FilterConditions();
    filterConditions.setStates(filterStates);

    // Filtering locations. This code allows only locations in NY and GA
    Filter.filterBrands(allBrands2, filterConditions);

    System.out.println("\n---Only locations in NY and GA");
    System.out.println(allBrands2);
  }

  private static void demonstratePrediction(ArrayList<Brand> allBrands,
      BrandDistances brandDistances, boolean detailedOutput) throws Exception {
    // Loading rental locations
    FileNames fileNames = new FileNames("rentalLocationsTest");

    // Calculating Diff
    LocationAnalysis locationAnalysis = new LocationAnalysis(allBrands, fileNames, brandDistances);

    System.out
        .println("##### PREDICTION \n---Ranking prospective rental locations to match the geographic pattern of stores in the area...\n");
    locationAnalysis.analyzeLocations(true);

    if (detailedOutput) {
      System.out.println("---Detailed Stats for McDonalds brand");
      System.out.println(brandDistances.getDetailedStats("McDonald's"));
    }
    
    // Creating an asciidoc report of the results and rendering to html
    RentalReport rentalReport = new RentalReport(locationAnalysis.getRentalLocations(), brandDistances);
  }

  private static void demonstrateSupportFunctions(ArrayList<Brand> allBrands) {
    // sample code to get a subset of brands. Can be used for plotting
    System.out
        .println("\n##### SUPPORT FUNCTIONS \n---For plotting, only of CVS and KFC Locations with lat/longs:");
    ArrayList<String> plotBrands = new ArrayList<String>();
    plotBrands.add("CVS");
    plotBrands.add("KFC");

    FilterConditions filterConditions = new FilterConditions(plotBrands);
    ArrayList<Brand> filteredBrands = Filter.cloneAndFilter(allBrands, filterConditions);
    System.out.println(filteredBrands);

    // sample code to get unique brands. Used to populate combo boxes
    System.out.println("\n--- For populating input boxes, list of unique brands:");
    ArrayList<String> uniqueBrandNames = Filter.getUniqueBrandNames(allBrands);
    Util.printList(uniqueBrandNames);
  }
}
