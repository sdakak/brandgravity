package com.sdakak.brandgravity;

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

// Lightweight class to encapsulate the average distance between brands
// author: sdakak
public class BrandPair implements Comparable<BrandPair> {
  private String brand1;
  private String brand2;
  private DescriptiveStatistics stats;

  public BrandPair(String brand1, String brand2) {
    this.brand1 = brand1;
    this.brand2 = brand2;
    this.stats = new DescriptiveStatistics();
  }

  public BrandPair() {
    this.brand1 = "BRAND1_NOT_SET";
    this.brand2 = "BRAND2_NOT_SET";
    this.stats = new DescriptiveStatistics();
  }

  public int compareTo(BrandPair compareBrandPair) {
    double compareDistance = compareBrandPair.getStats().getMean();

    // ascending order
    if (stats.getMean() > compareDistance)
      return 1;
    else
      return -1;
  }

  // TODO: Fix a bug in the following hack. Or don't write hacks like this
  // say we have brandPair = BrandPair ("Walmart","Walmart", -1);
  // then if we do brandPair.equals(others) - it gives the expected behavior
  // but if we do other.equal(brandPair) - it doesn't match
  public boolean equals(BrandPair compareBrandPair) {
    if ((brand1.equalsIgnoreCase(compareBrandPair.getBrand1()) && brand2
        .equalsIgnoreCase(compareBrandPair.getBrand2()))
        || (brand1.equalsIgnoreCase(compareBrandPair.getBrand2()) && brand2
            .equalsIgnoreCase(compareBrandPair.getBrand1())))
      return true;
    else
      return false;
  }

  public void addDistance(double distance) {
    stats.addValue(distance);
  }

  public void setBrand1(String brand1) {
    this.brand1 = brand1;
  }

  public void setBrand2(String brand2) {
    this.brand2 = brand2;
  }


  public DescriptiveStatistics getStats() {
    return stats;
  }

  public String getBrand1() {
    return brand1;
  }

  public String getBrand2() {
    return brand2;
  }

  @Override
  public String toString() {
    return brand1 + ", " + brand2 + ", " + getMean();
  }
  
  public Double getMean() {
  	return Util.prettyDouble(stats.getMean());
  }
  
  public Double getMedian() {
  	return Util.prettyDouble(stats.getPercentile(50));
  }

  public String getDetailedStats() {
    String result = brand1 + " and " + brand2 + " Stats:\n";
    result += "Mean distance: " + getMean() + "\n";
    result += "Median: " + getMedian() + "\n";
    result +=
        "Range: " + Util.prettyDouble(stats.getMax() - stats.getMin()) + " ("
            + Util.prettyDouble(stats.getMin()) + " - " + Util.prettyDouble(stats.getMax()) + ")\n";
    result +=
        "Interquartile range: "
            + Util.prettyDouble(stats.getPercentile(75) - stats.getPercentile(50)) + "\n";
    result += "Variance: " + Util.prettyDouble(stats.getVariance()) + "\n";
    result += "Standard Deviation: " + Util.prettyDouble(stats.getStandardDeviation()) + "\n";

    return result;
  }
}
