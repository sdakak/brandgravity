package com.sdakak.brandgravity;

// Lightweight class that contains the ordering of the various fields in the various files
// author: sdakak
public class FileFormat {
  // extension used for all files
  private final static String EXTENSION = ".csv";
  // private final String fileDirectory = "src/main/resources/";

  // position value of important fields in the input csv files
  private final static int INPUT_BRAND_NAME_COLUMN = 0;
  private final static int INPUT_LAT_LONG_COLUMN = 1;
  private final static int INPUT_STREET_ADDRESS_COLUMN = 2;
  private final static int INPUT_CITY_COLUMN = 3;
  private final static int INPUT_STATE_COLUMN = 4;
  private final static int INPUT_ZIP_COLUMN = 5;

  // for rental locations file
  private final static int RENTAL_NAME = 0;
  private final static int RENTAL_RENT = 1;
  private final static int RENTAL_BRAND_NAME_COLUMN = 2;
  private final static int RENTAL_LAT_LONG_COLUMN = 3;
  private final static int RENTAL_STREET_ADDRESS_COLUMN = 4;
  private final static int RENTAL_CITY_COLUMN = 5;
  private final static int RENTAL_STATE_COLUMN = 6;
  private final static int RENTAL_ZIP_COLUMN = 7;

  // position value of important fields in the permutation csv files
  private final static int PERMUTATION_FIRST_BRAND_NAME_COLUMN = 0;
  private final static int PERMUTATION_FIRST_BRAND_LAT_LONG_COLUMN = 1;
  private final static int PERMUTATION_SECOND_BRAND_NAME_COLUMN = 2;
  private final static int PERMUTATION_SECOND_BRAND_LAT_LONG_COLUMN = 3;
  private final static int CLOSEST_PAIR_SECOND_BRAND_NAME_COLUMN = 2;
  private final static int CLOSEST_PAIR_SECOND_BRAND_LAT_LONG_COLUMN = 3; 

  // position value of import fields in in the avgDistanceFile
  private final static int RESULTS_FIRST_BRAND_NAME_COLUMN = 0;
  private final static int RESULTS_SECOND_BRAND_NAME_COLUMN = 1;
  private final static int RESULTS_AVERAGE_DISTANCE_COLUMN = 2;

  // input filename
  private String locationFile;
  // intermediate filenames
  private String permutationFile, closestPairFile;
  // results filename
  private String avgDistanceFile;

  public static String getExtension() {
    return EXTENSION;
  }

  public static int getInputBrandNameColumn() {
    return INPUT_BRAND_NAME_COLUMN;
  }

  public static int getInputCityColumn() {
    return INPUT_CITY_COLUMN;
  }

  public static int getInputLatLongColumn() {
    return INPUT_LAT_LONG_COLUMN;
  }

  public static int getInputStateColumn() {
    return INPUT_STATE_COLUMN;
  }

  public static int getInputStreetAddressColumn() {
    return INPUT_STREET_ADDRESS_COLUMN;
  }

  public static int getInputZipColumn() {
    return INPUT_ZIP_COLUMN;
  }

  public static int getPermutationFirstBrandLatLongColumn() {
    return PERMUTATION_FIRST_BRAND_LAT_LONG_COLUMN;
  }

  public static int getPermutationFirstBrandNameColumn() {
    return PERMUTATION_FIRST_BRAND_NAME_COLUMN;
  }

  public static int getPermutationSecondBrandLatLongColumn() {
    return PERMUTATION_SECOND_BRAND_LAT_LONG_COLUMN;
  }

  public static int getPermutationSecondBrandNameColumn() {
    return PERMUTATION_SECOND_BRAND_NAME_COLUMN;
  }
  
  public static int getClosestPairSecondBrandNameColumn() {
	    return CLOSEST_PAIR_SECOND_BRAND_NAME_COLUMN;
  }
  
  public static int getClosestPairSecondBrandLatLongColumn() {
	    return CLOSEST_PAIR_SECOND_BRAND_LAT_LONG_COLUMN;
  }

  public static int getRentalBrandNameColumn() {
    return RENTAL_BRAND_NAME_COLUMN;
  }

  public static int getRentalCityColumn() {
    return RENTAL_CITY_COLUMN;
  }

  public static int getRentalLatLongColumn() {
    return RENTAL_LAT_LONG_COLUMN;
  }

  public static int getRentalName() {
    return RENTAL_NAME;
  }

  public static int getRentalRent() {
    return RENTAL_RENT;
  }

  public static int getRentalStateColumn() {
    return RENTAL_STATE_COLUMN;
  }

  public static int getRentalStreetAddressColumn() {
    return RENTAL_STREET_ADDRESS_COLUMN;
  }

  public static int getRentalZipColumn() {
    return RENTAL_ZIP_COLUMN;
  }

  public static int getResultsAverageDistanceColumn() {
    return RESULTS_AVERAGE_DISTANCE_COLUMN;
  }

  public static int getResultsFirstBrandNameColumn() {
    return RESULTS_FIRST_BRAND_NAME_COLUMN;
  }

  public static int getResultsSecondBrandNameColumn() {
    return RESULTS_SECOND_BRAND_NAME_COLUMN;
  }

  public String getAvgDistanceFile() {
    return avgDistanceFile;
  }

  public String getClosestPairFile() {
    return closestPairFile;
  }

  public String getLocationFile() {
    return locationFile;
  }

  public String getPermutationFile() {
    return permutationFile;
  }

}
