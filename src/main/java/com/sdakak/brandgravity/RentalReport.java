package com.sdakak.brandgravity;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import static org.asciidoctor.Asciidoctor.Factory.create;

import org.asciidoctor.Asciidoctor;
import org.asciidoctor.Attributes;
import org.asciidoctor.Options;

public class RentalReport {
	private ArrayList<Location> rentalLocations;
	private BrandDistances brandDistances;
	private String nl;

	public RentalReport(ArrayList<Location> rentalLocations,
			BrandDistances brandDistances) throws IOException {
		this.rentalLocations = rentalLocations;
		this.brandDistances = brandDistances;
		nl = System.getProperty("line.separator");

		createReport();
		renderReport();
	}

	private void createReport() throws IOException {
		String header = "= Prospective Rental Locations Report" + nl + 
		 "Generated automatically by BrandGravityAnalyzer <sdakak.zui@gmail.com>" + nl + nl;
		String introSection = createRentalLocationsIntroSection();
		String prerankingStub = 
				 "== Prospective Rental Locations in Ranked Order" + nl;
		String howRankedExplanation = "[NOTE]" + nl + "====" + nl +
				"How are the prospective rental locations ranked?" + nl + nl +
				"* Say you want to open a McDonalds franchise and you supplied the system with three prospective rental locations that you have scouted. The system will choose a location that is most similar to that of other McDonalds in the area. Consequently, your prospective rental locations are ranked in the order of 'most similar' location to 'least similar' as compared to other McDonalds." + nl + nl +
				"What is the Diff value calculated for each store?" + nl + nl +
				"* The Diff for each of your prospective rental location is how far off (in miles) your rental location is from it's 'ideal' spot. By putting your store in a location that is similar to where other McDonalds are (in this example), you place it in an area that is frequented by the same market segment that other McDonalds target. The fact that those other McDonalds are still in business means they are targeting the right segment." + nl +
				"====" + nl;
		
		PrintWriter printWriter = new PrintWriter("report.adoc", "US-ASCII");
		printWriter.println(header + introSection + prerankingStub + howRankedExplanation);
		
		Collections.sort(rentalLocations);
		// Generating sections for each of the prospective rental locations
		for (int i = 0; i < rentalLocations.size(); i ++) {
			Location location = rentalLocations.get(i);
			
			String locationTitle = "=== Rank " + (i+1) + ". " + location.getBrandName() + " in " + location.getRentalName() + ". Diff: " + location.getTotalDiff() + nl;
			String locationHeader =  "Rental Location Name: " + formatString(location.getRentalName(), "green") + nl + nl +
					"Rent: #$" + location.getRent() + "#" + nl + nl;
			
			String imageURL = ("image::" + makeVisualizationURL(location));
			String imageCaption = "[title=" + location.getBrandName() + " in " + location.getRentalName() + ", width=\"500\", height=\"500\", float=left, align=center]" + nl;
			
			String highMatchTable = createTable("High Match Due to These Brands", "red", location.getDiffMatrix().getClosestBrands());
			String lowMatchTable = createTable("Low Match Due to These Brands", "gray", location.getDiffMatrix().getFurthestBrands());
			
			String fontNote = mapLegend();
			
			String locationReport = locationTitle + locationHeader + imageURL + imageCaption + highMatchTable + lowMatchTable + fontNote;
			// For the first location include a note about how to read the map
			if ( i == 0 ) {
				String howReadMapExplanation = "[NOTE]" + nl + "====" + nl +
						"How do I interpret the maps visualization?" + nl + nl +
						"* The maps visualization shows your rental location (in [green]#green#) with lines originating from it to six other stores. " + nl + 
						"* The three [big]#big# markers in [red]#red# are the stores that are at similar distances from your rental location compared to how far they are from other McDonalds (continuing with the McDonalds example). " + nl +
						"** The [big]#big red# marker is the actual location of the store (for example a particular Arbys store). " + nl +
						"** The [small]#small red# marker is the average distance of this store (Arbys stores in general) from McDonalds stores. " + nl +
						"* The [gray]#gray# markers are the stores that are at most dissimilar distances." + nl + nl +
						"That's good. But I need to know more." + nl + nl +
						"* The [red]#red# stores are the positive indicators. They indicate that your rental location is in a similar location as compared to other McDonalds in the area. As such they contribute very little to the Diff. " + nl +
						"* The [gray]#gray# stores are negative indicators and contribute a lot to the Diff." + nl +
						"* The difference between the [big]#big# and [small]#small# markers on any path measures how 'off' your location is compared to that store. " + nl +
						"** Consequently, the [big]#big# and [small]#small# [red]#red# markers on each path are a smaller distance apart as compared to the [gray]#gray# ones. " + nl +
						"** This distance apart is tabulated to the right in the 'Off-by Distance' column" + nl + nl +
						"To know more about how to compare the prospective rental locations read the note at the end of this report." + nl +
						"====" + nl + nl;
				locationReport += howReadMapExplanation;
			}
			// For the last location include a note about how to compare the various rental locations
			if ( i == rentalLocations.size() - 1) {
				locationReport += "[NOTE]" + nl + "====" + nl +
						"OK all this is very fancy. But all I want is to open a new store. Do you have any tips on how to compare the three locations ranked?" + nl + nl +
						"* If you can afford the rent, you should choose the first rental location picked by the system. If not:" + nl +
						"** Compare the Diff values and think about the difference in rent. Is a lower ranked store sufficiently cheaper to be more profitable? " + nl + 
						"** Use your knowledge of the local neighborhoods to add another dimension to the Diff " + nl +
						"** How far off are the Diffs? Is that a meaningful difference compared to the size of neighbourhoods in the area: To target a particular clientele that visits a neighbourhood it maybe good enough to be anywhere in the neighborhood" + nl +
						"** Look at the stores that are your location's [red]#positive# and [gray]#negative# indicators. Do you think this type of store should affect your store's location. This can be counter intuitive. So be careful here. " + nl + 
						"*** You want both: stores that your clientele visits (for obvious reasons) and doesn't visit in the positive indicators. A store that your clientele doesn't visit as a positive indicator means that you are sufficiently far from a type of store you don't want to be near. For example, a Motel6 may indicate cheap real estate and you don't want your new luxury car show room beside it. " + nl +
						"*** If some stores show up as negative indicators that you don't think have any bearing on your clientele, you can give less weight to it. Though think very carefully before throwing that data point out. If a type of store is a certain distance away from all other franchises of your brand in the area it probably means something important." + nl +
						"====" + nl + nl;
			}
			printWriter.println(locationReport + nl + nl);
		}
		printWriter.close();
	}
	
	// Instructions on what the symbols on the maps mean
	private String mapLegend() {
		return "[NOTE]" + nl + "====" + nl + 
			  ". The [red]##c##[green]##o##[purple]##l##[fuchsia]##o##[blue]##r## of the markers is the color of the location shown above" + nl +
				". The **L**etter on the markers is the first letter of the locations shown above in bold" + nl +
				"====" + nl + nl;
	}
	
	// create asciidoc tables for High Match and Low Match brands
	private String createTable(String tableHeader, String color,
			ArrayList<BrandPair> brands) {
		// table header
		String table = "[options=\"header\", width=20]" + nl + "|===" + nl + "2+|"
				+ tableHeader + nl + "|Brand Name|Off-by distance (miles)" + nl;

		// brands to put in the cells of the table
		for (BrandPair currentBrandPair : brands) {
			table += "|" + formatString(currentBrandPair.getBrand2(), color) + "|"
					+ currentBrandPair.getMean() + nl;
		}

		// table footer
		table += "|===" + nl;
		return table;
	}
	
	// format a string to be a certain color and have the first letter bold
	private String formatString(String word, String color) {
		return "[" + color + "]#**" + word.charAt(0) + "**"
				+ word.substring(1, word.length()) + "#";
	}
	
	// Intro section contains the prospective rental location supplied by the user and a map showing them
	private String createRentalLocationsIntroSection() {
		String header = "You have supplied the system with the following prospective rental locations for a "
				+ rentalLocations.get(0).getBrandName() + " store." + nl + nl;
				
		String prefix = "https://maps.googleapis.com/maps/api/staticmap?size=500x500&key=AIzaSyC3DxrYiN6V9x9BZWQCCMYLNrYGwbeAIE4&maptype=terrain";
		String mapURL = prefix;
		// will contain the three rental location names
		String threeLocations = "";
		
		// Populate marURL and threeLocations
		for (Location location : rentalLocations) {
			String rentalLocation_prefix = "&markers=color:green%7Clabel:"
					+ Character.toUpperCase(location.getRentalName().charAt(0));
			mapURL += rentalLocation_prefix + "%7C"
					+ location.getAddress().getStringCoordinates();

			threeLocations += ". " + formatString(location.getRentalName(), "green")
					+ nl;
		}
		
		String imageCaption = "[title=Showing the prospective rental locations, width=\"500\", height=\"500\", float = left, align = center]" + nl;
		String imageSection = "image::" + mapURL + imageCaption + nl;
		
		return header + imageSection + threeLocations + nl + mapLegend();
	}
    
	// render the asciidoc text fil ecreated by createReport in an html page
	private void renderReport() throws IOException {
		Asciidoctor asciidoctor = create();

		// Specify the styling attributes
		Attributes attributes = new Attributes();
		attributes.setBackend("html");
		attributes.setStyleSheetName("github.css");
		Options options = new Options();
		options.setAttributes(attributes);
		options.setHeaderFooter(true);
		// options.setInPlace(true);

		FileReader reader = new FileReader(new File("report.adoc"));
		PrintWriter printWriter = new PrintWriter("report.html",
				"UTF-8");

		asciidoctor.convert(reader, printWriter, options);
		printWriter.close();
	}

	// Make the google maps URL analyzing the ranking of a prospective rental location
	private String makeVisualizationURL(Location location) {
		String url = "";

		// prefix
		String prefix = "https://maps.googleapis.com/maps/api/staticmap?size=500x500&key=AIzaSyC3DxrYiN6V9x9BZWQCCMYLNrYGwbeAIE4&maptype=terrain";
		String rentalLocation_prefix = "&markers=color:green%7Clabel:"
				+ Character.toUpperCase(location.getRentalName().charAt(0));
		url += prefix + rentalLocation_prefix + "%7C"
				+ location.getAddress().getStringCoordinates();

		// markers
		String closest_prefix = "&markers=color:red%7Clabel:";
		ArrayList<BrandPair> closestBrands = location.getDiffMatrix()
				.getClosestBrands();
		for (BrandPair brandPair : closestBrands) {
			url += closest_prefix
					+ Character.toUpperCase(brandPair.getBrand2().charAt(0)) + "%7C"
					+ location.findStore(brandPair.getBrand2());
		}
		String furthest_prefix = "&markers=color:black%7Clabel:";
		ArrayList<BrandPair> furthestBrands = location.getDiffMatrix()
				.getFurthestBrands();
		for (BrandPair brandPair : furthestBrands) {
			url += furthest_prefix
					+ Character.toUpperCase(brandPair.getBrand2().charAt(0)) + "%7C"
					+ location.findStore(brandPair.getBrand2());
		}

		String closest_avg_distance_prefix = "&markers=color:red%7Csize:tiny";
		url += closest_avg_distance_prefix;
		for (BrandPair brandPair : closestBrands) {
			url += "%7C"
					+ Geo.getPointOnPathString(
							location.getAddress().getCoordinates(),
							Geo.stringToLatLng(location.findStore(brandPair.getBrand2())),
							brandDistances.getDistance(brandPair.getBrand1(),
									brandPair.getBrand2()));
		}
		String furthest_avg_distance_prefix = "&markers=color:black%7Csize:tiny";
		url += furthest_avg_distance_prefix;
		for (BrandPair brandPair : furthestBrands) {
			url += "%7C"
					+ Geo.getPointOnPathString(
							location.getAddress().getCoordinates(),
							Geo.stringToLatLng(location.findStore(brandPair.getBrand2())),
							brandDistances.getDistance(brandPair.getBrand1(),
									brandPair.getBrand2()));
		}

		// paths
		String closest_path_prefix = "&path=color:red%7Cweight:2";
		url += closest_path_prefix;
		for (BrandPair brandPair : closestBrands) {
			url += "%7C"
					+ location.getAddress().getStringCoordinates()
					+ "%7C"
					+ location.findStore(brandPair.getBrand2())
					+ "%7C"
					+ Geo.getPointOnPathString(
							location.getAddress().getCoordinates(),
							Geo.stringToLatLng(location.findStore(brandPair.getBrand2())),
							brandDistances.getDistance(brandPair.getBrand1(),
									brandPair.getBrand2()));
		}
		String furthest_path_prefix = "&path=color:black%7Cweight:2";
		url += furthest_path_prefix;
		for (BrandPair brandPair : furthestBrands) {
			url += "%7C"
					+ location.getAddress().getStringCoordinates()
					+ "%7C"
					+ location.findStore(brandPair.getBrand2())
					+ "%7C"
					+ Geo.getPointOnPathString(
							location.getAddress().getCoordinates(),
							Geo.stringToLatLng(location.findStore(brandPair.getBrand2())),
							brandDistances.getDistance(brandPair.getBrand1(),
									brandPair.getBrand2()));
		}
		return url;
	}

}
