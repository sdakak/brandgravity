package com.sdakak.brandgravity;

import java.util.ArrayList;
import java.util.Iterator;

import junit.framework.TestCase;

public class LocationAnalysisTest extends TestCase {
  ArrayList<Brand> allBrands;
  ArrayList<Location> rentalLocations;
  FileNames storeFileNames;
  FileNames rentalFileNames;

  private void initialize() throws Exception {
    this.storeFileNames = new FileNames("storesTest");
    this.allBrands = Reader.readInputFile(storeFileNames.getInputFile());

    this.rentalFileNames = new FileNames("predictionTest");
    this.rentalLocations = Reader.readRentalLocationFile(rentalFileNames.getInputFile());
  }

  // There are three specific stores in the philly area specified in testPrediction.csv. For each
  // of them, this test first removes it from the world. Then it adds it as a prospective
  // rentalLocation and calculates the Diff. It then compares that Diff with a manually calculated
  // Diff to make sure the Prediction algorithm is working as expected
  public void testRentalLocationDiff() throws Exception {
    initialize();
    for (Location currentLocation : rentalLocations) {
      double diff =
          DiffPerBrand.calculateDiffForExistingStore(currentLocation, allBrands, storeFileNames,
              rentalFileNames, false);

      if (currentLocation.getAddress().equals(new Address("39.9393790,-75.1667580"))) {
        assertEquals(5.01, diff);
        System.out.println("Diff as expected on mcd_sbroadst_philly");
      } else if (currentLocation.getAddress().equals(new Address("39.9224320,-75.1696790"))) {
        assertEquals(5.22, diff);
        System.out.println("Diff as expected on subway_broadst_philly");
      } else if (currentLocation.getAddress().equals(new Address("39.9716730,-75.1591900"))) {
        assertEquals(6.24, diff);
        System.out.println("Diff as expected on mcd_nbraodst_philly");
      } else if (currentLocation.getAddress().equals(new Address("39.9541500,-75.2020040"))) {
        assertEquals(8.96, diff);
        System.out.println("Diff as expected on cvs_walnutst_philly");
      }

    }
  }
}
