package com.sdakak.brandgravity;

import java.util.ArrayList;

import junit.framework.TestCase;

public class CombinatoricsTest extends TestCase {
  private BrandDistances brandDistances;

  private void initialize() throws Exception { 
    FileNames fileNames = new FileNames("storesTest");
    final ArrayList<Brand> allBrands = Reader.readInputFile(fileNames.getInputFile());
    this.brandDistances = Combinatorics.calculateAverageDistance(allBrands, fileNames, false);
  }

  public void testGetAvgDistance() throws Exception {
    initialize();
    assertEquals(1.2334074320176012, brandDistances.getDistance("McDonald's", "McDonald's"), 0.01);
    assertEquals(1.0990440083749193, brandDistances.getDistance("McDonald's", "KFC"), 0.01);
    assertEquals(0.9812419283811751, brandDistances.getDistance("McDonald's", "CVS"), 0.01);
    assertEquals(1.0254942747857405, brandDistances.getDistance("McDonald's", "Subway"), 0.01);
    assertEquals(1.0086261481824736, brandDistances.getDistance("McDonald's", "StarBucks"), 0.01);
    assertEquals(6.361747542845881, brandDistances.getDistance("McDonald's", "Walmart"), 0.01);
    assertEquals(0.9016664815476509, brandDistances.getDistance("McDonald's", "Wendys"), 0.01);
    assertEquals(3.4717329542295374, brandDistances.getDistance("McDonald's", "Arbys"), 0.01);
    assertEquals(1.3183247215143603, brandDistances.getDistance("McDonald's", "AppleBees"), 0.01);
    assertEquals(1.2466958222959361, brandDistances.getDistance("McDonald's", "Domino's"), 0.01);
    assertEquals(3.142406815294352, brandDistances.getDistance("KFC", "KFC"), 0.01);
    assertEquals(4.614085707044639, brandDistances.getDistance("KFC", "CVS"), 0.01);
    assertEquals(2.7822318799553964, brandDistances.getDistance("KFC", "Subway"), 0.01);
    assertEquals(4.736829106977725, brandDistances.getDistance("KFC", "StarBucks"), 0.01);
    assertEquals(4.95178436606959, brandDistances.getDistance("KFC", "Walmart"), 0.01);
    assertEquals(2.697450187268998, brandDistances.getDistance("KFC", "Wendys"), 0.01);
    assertEquals(3.6728439727841846, brandDistances.getDistance("KFC", "Arbys"), 0.01);
    assertEquals(3.280784909514266, brandDistances.getDistance("KFC", "AppleBees"), 0.01);
    assertEquals(2.401722447993067, brandDistances.getDistance("KFC", "Domino's"), 0.01);
    assertEquals(2.0026444821210685, brandDistances.getDistance("CVS", "CVS"), 0.01);
    assertEquals(1.4382232987778993, brandDistances.getDistance("CVS", "Subway"), 0.01);
    assertEquals(2.013045518390912, brandDistances.getDistance("CVS", "StarBucks"), 0.01);
    assertEquals(6.932825376501458, brandDistances.getDistance("CVS", "Walmart"), 0.01);
    assertEquals(1.835364788466069, brandDistances.getDistance("CVS", "Wendys"), 0.01);
    assertEquals(4.75801458262394, brandDistances.getDistance("CVS", "Arbys"), 0.01);
    assertEquals(1.5622991207311325, brandDistances.getDistance("CVS", "AppleBees"), 0.01);
    assertEquals(2.0649644559061775, brandDistances.getDistance("CVS", "Domino's"), 0.01);
    assertEquals(1.9149740424363841, brandDistances.getDistance("Subway", "Subway"), 0.01);
    assertEquals(2.9654651032944797, brandDistances.getDistance("Subway", "StarBucks"), 0.01);
    assertEquals(5.678253322578501, brandDistances.getDistance("Subway", "Walmart"), 0.01);
    assertEquals(1.9005276526440185, brandDistances.getDistance("Subway", "Wendys"), 0.01);
    assertEquals(3.802762649374943, brandDistances.getDistance("Subway", "Arbys"), 0.01);
    assertEquals(2.1354893737048686, brandDistances.getDistance("Subway", "AppleBees"), 0.01);
    assertEquals(1.4016342538234337, brandDistances.getDistance("Subway", "Domino's"), 0.01);
    assertEquals(0.27881711682672183, brandDistances.getDistance("StarBucks", "StarBucks"), 0.01);
    assertEquals(6.537248514755521, brandDistances.getDistance("StarBucks", "Walmart"), 0.01);
    assertEquals(0.5420687565785643, brandDistances.getDistance("StarBucks", "Wendys"), 0.01);
    assertEquals(3.228709296534549, brandDistances.getDistance("StarBucks", "Arbys"), 0.01);
    assertEquals(0.6894676188408556, brandDistances.getDistance("StarBucks", "AppleBees"), 0.01);
    assertEquals(0.7640660478970414, brandDistances.getDistance("StarBucks", "Domino's"), 0.01);
    assertEquals(6.3785552155411, brandDistances.getDistance("Walmart", "Walmart"), 0.01);
    assertEquals(6.361935804716412, brandDistances.getDistance("Walmart", "Wendys"), 0.01);
    assertEquals(4.01547520478436, brandDistances.getDistance("Walmart", "Arbys"), 0.01);
    assertEquals(6.519571070570261, brandDistances.getDistance("Walmart", "AppleBees"), 0.01);
    assertEquals(6.535337880284823, brandDistances.getDistance("Walmart", "Domino's"), 0.01);
    assertEquals(3.5446260184013996, brandDistances.getDistance("Wendys", "Wendys"), 0.01);
    assertEquals(4.859218337263985, brandDistances.getDistance("Wendys", "Arbys"), 0.01);
    assertEquals(2.0501888047173664, brandDistances.getDistance("Wendys", "AppleBees"), 0.01);
    assertEquals(3.038447762005303, brandDistances.getDistance("Wendys", "Domino's"), 0.01);
    assertEquals(4.688638994415665, brandDistances.getDistance("Arbys", "Arbys"), 0.01);
    assertEquals(5.414557368504492, brandDistances.getDistance("Arbys", "AppleBees"), 0.01);
    assertEquals(4.571140437526018, brandDistances.getDistance("Arbys", "Domino's"), 0.01);
    assertEquals(4.176950634938542, brandDistances.getDistance("AppleBees", "AppleBees"), 0.01);
    assertEquals(4.041602857425168, brandDistances.getDistance("AppleBees", "Domino's"), 0.01);
    assertEquals(4.399616078603833, brandDistances.getDistance("Domino's", "Domino's"), 0.01);
  }
}
